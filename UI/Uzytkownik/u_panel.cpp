#include "u_panel.h"
#include "ui_u_panel.h"
#include <UI/nowawiadomosc.h>
#include <UI/Uzytkownik/u_zwrocfilm.h>
#include <UI/Model/film.h>
#include <UI/Model/mojapolka.h>
#include <UI/Model/historiauzytkownika.h>
#include <UI/Model/listawiadomosciuzytkownika.h>
#include <QMessageBox>
#include <QDebug>

namespace UI { namespace Uzytkownik {

Panel::Panel(BL::BazaDanych &bazaDanych, std::shared_ptr<BL::Model::Uzytkownik> uzytkownik, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Panel),
    m_BazaDanych(bazaDanych),
    m_Uzytkownik(uzytkownik)
{
    ui->setupUi(this);
    RefreshViews();
    ui->wiadomosciWybrana->hide();
}

Panel::~Panel()
{
    delete ui;
}

void Panel::on_KatalogFilmow_clicked(const QModelIndex &index)
{
    if (auto film = m_BazaDanych.FilmPoNumerzeWiersza(index.row()))
    {
        ui->katalogObsada->setText("Obsada: " + film->Obsada());
        ui->katalogOpis->setText(film->Opis());
        ui->katalogRezyser->setText("Reżyser: " + film->Rezyser());

        // Zaokrąglij do 2 miejsc po przecinku
        float ocena = std::round(film->Ocena() * 100) / 100.0f;
        if (ocena > 0)
            ui->katalogOcena->setText(QString("Ocena: ") + QString::number(ocena) + "/10");
        else
            ui->katalogOcena->setText("Brak ocen");
    }
}

void Panel::on_KatalogFilmow_doubleClicked(const QModelIndex &index)
{
    auto film = m_BazaDanych.FilmPoNumerzeWiersza(index.row());
    // Nie można wypożyczyć nieistniejącego lub już wypożyczonego filmu
    if (! film || film->Wypozyczajacy())
        return;

    auto reply = QMessageBox::question(this, "Wypożyczenie filmu", "Czy na pewno chcesz wypożyczyć tą pozycję?", QMessageBox::Yes, QMessageBox::No);
    if (reply == QMessageBox::Yes)
        on_katalogWypozycz_clicked();
}

void Panel::on_katalogWypozycz_clicked()
{
    if (! ui->KatalogFilmow->selectionModel()->hasSelection())
        return;

    int row = ui->KatalogFilmow->selectionModel()->selectedRows().at(0).row();

    if (auto film = m_BazaDanych.FilmPoNumerzeWiersza(row))
    {
        try
        {
            film->Wypozycz(m_Uzytkownik);
            m_BazaDanych.Zapisz();
            RefreshViews();
        }
        catch(std::exception& ex)
        {
            QMessageBox::critical(this, "Błąd", ex.what());
        }
    }
}

void Panel::on_polkaTabela_clicked(const QModelIndex &index)
{
    if (auto film = m_BazaDanych.FilmPoNumerzeWiersza(index.row()))
    {
        ui->polkaObsada->setText("Obsada: " + film->Obsada());
        ui->polkaOpis->setText(film->Opis());
        ui->polkaRezyser->setText("Reżyser: " + film->Rezyser());
    }
}

void Panel::on_polkaZwroc_clicked()
{
    if (! ui->polkaTabela->selectionModel()->hasSelection())
        return;

    int row = ui->polkaTabela->selectionModel()->selectedRows().at(0).row();

    if (auto film = m_Uzytkownik->Filmy().at(row))
    {
        ZwrocFilm dialog(film);

        if (dialog.exec() != QDialog::Accepted)
            return;
        try
        {
            film->Zwroc(dialog.Ocena(), dialog.Recenzja());
            m_BazaDanych.Zapisz();
            RefreshViews();
        }
        catch(std::exception& ex)
        {
            QMessageBox::critical(this, "Błąd", ex.what());
        }
    }
}

void Panel::RefreshViews()
{
    ui->KatalogFilmow->setModel(new UI::Model::Film(m_BazaDanych));
    ui->polkaTabela->setModel(new UI::Model::MojaPolka(m_Uzytkownik));
    ui->historiaTabela->setModel(new UI::Model::HistoriaUzytkownika(m_Uzytkownik));
    ui->wiadomosciLista->setModel(new UI::Model::ListaWiadomosciUzytkownika(m_Uzytkownik));
}

void Panel::on_polkaTabela_doubleClicked(const QModelIndex &index)
{
    index.isValid(); // Eliminuje ostrzeżenie o nieużywanym argumencie
    auto reply = QMessageBox::question(this, "Zwrot filmu", "Czy na pewno chcesz zwrócić tą pozycję?", QMessageBox::Yes, QMessageBox::No);
    if (reply == QMessageBox::Yes)
        on_polkaZwroc_clicked();
}

void Panel::on_wiadomosciNowa_clicked()
{
    NowaWiadomosc dialog(m_BazaDanych);
    dialog.Pracownik(std::shared_ptr<BL::Model::Pracownik>());
    if (dialog.exec() != QDialog::Accepted)
        return;

    try
    {
        m_BazaDanych.WyslijWiadomosc(m_Uzytkownik, dialog.Pracownik(), DA::Model::Wiadomosc::DO_PRACOWNIKA, dialog.Temat(), dialog.Tresc());
        m_BazaDanych.Zapisz();
        RefreshViews();
    }
    catch(std::exception& ex)
    {
        QMessageBox::critical(this, "Błąd", ex.what());
    }
}

void Panel::on_wiadomosciLista_clicked(const QModelIndex &index)
{
    auto wiadomosc = m_Uzytkownik->Wiadomosci().at(index.row());
    if (! wiadomosc)
    {
        ui->wiadomosciWybrana->hide();
        ui->wiadomosciOdpowiedz->setEnabled(false);
        return;
    }

    ui->wiadomosciWybrana->show();

    ui->wiadomosciOdpowiedz->setEnabled(true);

    ui->wiadomosciTemat->setText(wiadomosc->Temat());
    ui->wiadomosciTresc->setPlainText(wiadomosc->Tresc());

    if (wiadomosc->Kierunek() == DA::Model::Wiadomosc::DO_UZYTKOWNIKA)
        ui->wiadomoscOdDo->setText("Od: " + wiadomosc->Pracownik()->ImieNazwisko());
    else
        ui->wiadomoscOdDo->setText("Do: " + wiadomosc->Pracownik()->ImieNazwisko());
}

void Panel::on_wiadomosciOdpowiedz_clicked()
{
    auto wiadomosc = m_Uzytkownik->Wiadomosci().at(ui->wiadomosciLista->currentIndex().row());

    NowaWiadomosc dialog(m_BazaDanych);
    dialog.Pracownik(wiadomosc->Pracownik());
    dialog.Temat("RE: " + wiadomosc->Temat());

    if (dialog.exec() != QDialog::Accepted)
        return;
    try
    {
        m_BazaDanych.WyslijWiadomosc(m_Uzytkownik, dialog.Pracownik(), DA::Model::Wiadomosc::DO_PRACOWNIKA, dialog.Temat(), dialog.Tresc());
        m_BazaDanych.Zapisz();
        RefreshViews();
    }
    catch(std::exception& ex)
    {
        QMessageBox::critical(this, "Błąd", ex.what());
    }
}

void Panel::on_kontoZmienHaslo_clicked()
{
    if (! m_Uzytkownik->SprawdzHaslo(ui->kontoHaslo->text()))
    {
        QMessageBox::warning(this, "Błędne dane", "Nieprawidłowe hasło!");
        return;
    }

    if (ui->kontoNoweHaslo->text().trimmed() == "")
    {
        QMessageBox::warning(this, "Błędne dane", "Nowe hasło nie może być puste!");
        return;
    }

    if (ui->kontoNoweHaslo->text() != ui->kontoPowtorzHaslo->text())
    {
        QMessageBox::warning(this, "Błędne dane", "Podane hasła się różnią!");
        return;
    }

    try
    {
        m_Uzytkownik->ZmienHaslo(ui->kontoNoweHaslo->text());
        m_BazaDanych.Zapisz();
        ui->kontoHaslo->setText("");
        ui->kontoNoweHaslo->setText("");
        ui->kontoPowtorzHaslo->setText("");
    }
    catch(std::exception& ex)
    {
        QMessageBox::critical(this, "Błąd", ex.what());
    }
}

}}
