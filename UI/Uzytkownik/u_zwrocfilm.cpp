#include "u_zwrocfilm.h"
#include "ui_u_zwrocfilm.h"

namespace UI { namespace Uzytkownik {

ZwrocFilm::ZwrocFilm(std::shared_ptr<BL::Model::Film> film, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ZwrocFilm),
    m_film(film)
{
    ui->setupUi(this);
    ui->tytul->setText("Zwracasz film " + m_film->Tytul());
}

ZwrocFilm::~ZwrocFilm()
{
    delete ui;
}

int ZwrocFilm::Ocena()
{
    return ui->ocena->value();
}

QString ZwrocFilm::Recenzja()
{
    return ui->recenzja->toPlainText();
}

}}

