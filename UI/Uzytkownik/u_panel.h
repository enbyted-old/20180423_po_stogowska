#pragma once

#include <QDialog>
#include <BL/Model/uzytkownik.h>
#include <BL/bazadanych.h>
#include <memory>

namespace UI {
namespace Uzytkownik {

namespace Ui {
class Panel;
}

class Panel : public QDialog
{
    Q_OBJECT
public:
    explicit Panel(BL::BazaDanych& bazaDanych, std::shared_ptr<BL::Model::Uzytkownik> uzytkownik, QWidget *parent = 0);
    ~Panel();

private slots:
    void on_KatalogFilmow_clicked(const QModelIndex &index);

    void on_KatalogFilmow_doubleClicked(const QModelIndex &index);

    void on_katalogWypozycz_clicked();

    void on_polkaTabela_clicked(const QModelIndex &index);

    void on_polkaZwroc_clicked();

    void on_polkaTabela_doubleClicked(const QModelIndex &index);

    void on_wiadomosciNowa_clicked();

    void on_wiadomosciLista_clicked(const QModelIndex &index);

    void on_wiadomosciOdpowiedz_clicked();

    void on_kontoZmienHaslo_clicked();

private:
    void RefreshViews();
private:
    Ui::Panel *ui;
    BL::BazaDanych& m_BazaDanych;
    std::shared_ptr<BL::Model::Uzytkownik> m_Uzytkownik;
};


} // namespace Uzytkownik
} // namespace UI
