#pragma once

#include <QDialog>
#include <QString>

namespace UI {
namespace Uzytkownik {

namespace Ui {
class Logowanie;
}

class Logowanie : public QDialog
{
    Q_OBJECT

public:
    explicit Logowanie(QWidget *parent = 0);
    ~Logowanie();
    QString Login() const;
    QString Haslo() const;

private:
    Ui::Logowanie *ui;
};


} // namespace Uzytkownik
} // namespace UI

