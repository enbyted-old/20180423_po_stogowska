#pragma once

#include <QDialog>
#include <BL/Model/film.h>

namespace UI { namespace Uzytkownik {

namespace Ui {
class ZwrocFilm;
}

class ZwrocFilm : public QDialog
{
    Q_OBJECT

public:
    explicit ZwrocFilm(std::shared_ptr<BL::Model::Film> film, QWidget *parent = 0);
    ~ZwrocFilm();

    int Ocena();
    QString Recenzja();

private:
    Ui::ZwrocFilm *ui;
    std::shared_ptr<BL::Model::Film> m_film;
};


}}
