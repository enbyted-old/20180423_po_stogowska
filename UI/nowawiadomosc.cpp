#include "nowawiadomosc.h"
#include "ui_nowawiadomosc.h"
#include <QMessageBox>
#include <UI/Model/listapracownikow.h>
#include <UI/Model/listauzytkownikow.h>

namespace UI {

NowaWiadomosc::NowaWiadomosc(BL::BazaDanych& bazaDanych, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NowaWiadomosc),
    m_BazaDanych(bazaDanych)
{
    ui->setupUi(this);
}

NowaWiadomosc::~NowaWiadomosc()
{
    delete ui;
}

void NowaWiadomosc::Pracownik(std::shared_ptr<BL::Model::Pracownik> pracownik)
{
    ui->adresat->setModel(new UI::Model::ListaPracownikow(m_BazaDanych));
    ui->adresat->setCurrentIndex(m_BazaDanych.IndeksPracownika(pracownik));
}

void NowaWiadomosc::Uzytkownik(std::shared_ptr<BL::Model::Uzytkownik> uzytkownik)
{
    ui->adresat->setModel(new UI::Model::ListaUzytkownikow(m_BazaDanych));
    ui->adresat->setCurrentIndex(m_BazaDanych.IndeksUzytkownika(uzytkownik));
}

void NowaWiadomosc::Temat(const QString &temat)
{
    ui->temat->setText(temat);
}

QString NowaWiadomosc::Temat() const
{
    return ui->temat->text();
}

QString NowaWiadomosc::Tresc() const
{
    return ui->tresc->toPlainText();
}

std::shared_ptr<BL::Model::Pracownik> NowaWiadomosc::Pracownik() const
{
    return m_BazaDanych.PracownikPoNumerzeWiersza(ui->adresat->currentIndex());
}

std::shared_ptr<BL::Model::Uzytkownik> NowaWiadomosc::Uzytkownik() const
{
    return m_BazaDanych.UzytkownikPoNumerzeWiersza(ui->adresat->currentIndex());
}

void NowaWiadomosc::on_buttonBox_accepted()
{
    if (ui->adresat->currentIndex() < 0)
    {
        QMessageBox::warning(this, "Błąd", "Musisz wybrać adresata!");
        return;
    }

    if (ui->temat->text().trimmed() == "")
    {
        QMessageBox::warning(this, "Błąd", "Temat nie może być pusty!");
        return;
    }

    if (ui->tresc->toPlainText().trimmed() == "")
    {
        QMessageBox::warning(this, "Błąd", "Wiadomość musoi zawierać treść!");
        return;
    }

    accept();
}

}
