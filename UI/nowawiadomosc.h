#pragma once

#include <QDialog>
#include <BL/bazadanych.h>

namespace UI {

namespace Ui {
class NowaWiadomosc;
}

class NowaWiadomosc : public QDialog
{
    Q_OBJECT

public:
    explicit NowaWiadomosc(BL::BazaDanych& bazaDanych, QWidget *parent = 0);
    ~NowaWiadomosc();

    void Pracownik(std::shared_ptr<BL::Model::Pracownik> pracownik);
    void Uzytkownik(std::shared_ptr<BL::Model::Uzytkownik> uzytkownik);
    void Temat(const QString& temat);

    QString Temat() const;
    QString Tresc() const;
    std::shared_ptr<BL::Model::Pracownik> Pracownik() const;
    std::shared_ptr<BL::Model::Uzytkownik> Uzytkownik() const;

private slots:
    void on_buttonBox_accepted();

private:
    Ui::NowaWiadomosc *ui;
    BL::BazaDanych& m_BazaDanych;
};

} // namespace UI
