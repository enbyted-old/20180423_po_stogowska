#pragma once

#include <QDialog>
#include <BL/bazadanych.h>

namespace UI {
namespace Gosc {

namespace Ui {
class Panel;
}

class Panel : public QDialog
{
    Q_OBJECT

public:
    explicit Panel(BL::BazaDanych& bazaDanych, QWidget *parent = 0);
    ~Panel();

private slots:
    void on_tabelaFilmow_activated(const QModelIndex &index);

    void on_tabelaFilmow_clicked(const QModelIndex &index);

    void on_pushButton_clicked();

private:
    Ui::Panel *ui;
    BL::BazaDanych& m_BazaDanych;
};


} // namespace Gosc
} // namespace UI

