#include "g_panel.h"
#include "ui_g_panel.h"
#include <UI/Gosc/g_register.h>
#include <UI/Model/film.h>
#include <QMessageBox>

namespace UI {
namespace Gosc {

Panel::Panel(BL::BazaDanych& bazaDanych, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Panel),
    m_BazaDanych(bazaDanych)
{
    ui->setupUi(this);
    ui->tabelaFilmow->setModel(new UI::Model::Film(m_BazaDanych));
    ui->lblObsada->setText("");
    ui->lblOpis->setText("");
    ui->lblRezyser->setText("");
}

Panel::~Panel()
{
    delete ui;
}

}}

void UI::Gosc::Panel::on_tabelaFilmow_activated(const QModelIndex &index)
{
    index.isValid(); // Eliminuje ostrzeżenie o nieużywanym argumencie
    QMessageBox(QMessageBox::Information, "Działanie niedostępne", "Tylko zalogowani użytkownicy mogą wypozyczać filmy").exec();
}

void UI::Gosc::Panel::on_tabelaFilmow_clicked(const QModelIndex &index)
{
    if (auto film = m_BazaDanych.FilmPoNumerzeWiersza(index.row()))
    {
        ui->lblObsada->setText("Obsada: " + film->Obsada());
        ui->lblOpis->setText(film->Opis());
        ui->lblRezyser->setText("Reżyser: " + film->Rezyser());
    }
}

void UI::Gosc::Panel::on_pushButton_clicked()
{
    if (Register(m_BazaDanych).exec() == Register::Accepted)
    {
        QMessageBox(QMessageBox::Information, "Rejestracja zakończona", "Teraz możesz się zalogować jako użytkownik.");
        close();
    }
}
