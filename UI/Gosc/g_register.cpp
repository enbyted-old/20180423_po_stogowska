#include "g_register.h"
#include "ui_g_register.h"
#include <QMessageBox>
#include <stdexcept>

namespace UI {
namespace Gosc {

Register::Register(BL::BazaDanych& bazaDanych, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Register),
    m_BazaDanych(bazaDanych)
{
    ui->setupUi(this);
}

Register::~Register()
{
    delete ui;
}

} // namespace Gosc
} // namespace UI

void UI::Gosc::Register::on_buttonBox_accepted()
{
    if (ui->txtHaslo->text() != ui->txtPowtorzHaslo->text())
    {
        QMessageBox(QMessageBox::Critical, "Niepoprawne dane", "Podane hasła się różnią!").exec();
        return;
    }

    try
    {
        m_BazaDanych.RejestrujUzytkownika(
            ui->txtLogin->text(),
            ui->txtHaslo->text(),
            ui->txtImie->text(),
            ui->txtNazwisko->text()
        );
        m_BazaDanych.Zapisz();
    }
    catch(std::runtime_error& err)
    {
        QMessageBox(QMessageBox::Critical, "Niepoprawne dane", err.what()).exec();
        return;
    }

    accept();
}
