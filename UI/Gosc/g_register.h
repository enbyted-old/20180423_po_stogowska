#pragma once

#include <QDialog>
#include <BL/bazadanych.h>

namespace UI {
namespace Gosc {

namespace Ui {
class Register;
}

class Register : public QDialog
{
    Q_OBJECT

public:
    explicit Register(BL::BazaDanych& bazaDanych, QWidget *parent = 0);
    ~Register();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::Register *ui;
    BL::BazaDanych& m_BazaDanych;
};


} // namespace Gosc
} // namespace UI
