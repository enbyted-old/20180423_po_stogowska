#pragma once
#include <QDialog>

#include <BL/bazadanych.h>

namespace UI {

namespace Ui {
class WybierzTryb;
}

class WybierzTryb : public QDialog
{
    Q_OBJECT

public:
    explicit WybierzTryb(BL::BazaDanych& bazaDanych, QWidget *parent = 0);
    ~WybierzTryb();

private slots:
    void on_btnGosc_clicked();

    void on_btnUzytkownik_clicked();

    void on_btnPracownik_clicked();

private:
    Ui::WybierzTryb *ui;
    BL::BazaDanych& m_BazaDanych;
};


} // namespace UI
