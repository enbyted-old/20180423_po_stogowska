#include "listawiadomoscipracownika.h"
#include <BL/Model/wiadomosc.h>
#include <BL/Model/uzytkownik.h>

namespace UI { namespace Model {

ListaWiadomosciPracownika::ListaWiadomosciPracownika(std::shared_ptr<BL::Model::Pracownik> pracownik, QObject *parent)
    : QAbstractListModel(parent)
    , m_pracownik(pracownik)
{
}

QVariant ListaWiadomosciPracownika::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation != Qt::Horizontal)
        return QVariant();

    if (section != 0)
        return QVariant();

    return "Wiadomości";
}

int ListaWiadomosciPracownika::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_pracownik->Wiadomosci().size();
}

QVariant ListaWiadomosciPracownika::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    auto wiadomosc = m_pracownik->Wiadomosci().at(index.row());

    if (! wiadomosc)
        return QVariant();

    if (wiadomosc->Kierunek() == DA::Model::Wiadomosc::DO_UZYTKOWNIKA)
    {
        return (
            wiadomosc->Pracownik()->ImieNazwisko() + " --> " + wiadomosc->Uzytkownik()->ImieNazwisko() +
            " \"" + wiadomosc->Temat() + "\""
        );
    }
    else // Do pracownika
    {
        return (
            wiadomosc->Uzytkownik()->ImieNazwisko() + " --> " + wiadomosc->Pracownik()->ImieNazwisko() +
            " \"" + wiadomosc->Temat() + "\""
        );
    }
}

}}
