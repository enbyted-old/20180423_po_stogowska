#pragma once

#include <QAbstractListModel>
#include <BL/Model/pracownik.h>

namespace UI { namespace Model {

class ListaWiadomosciPracownika : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ListaWiadomosciPracownika(std::shared_ptr<BL::Model::Pracownik> pracownik, QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    std::shared_ptr<BL::Model::Pracownik> m_pracownik;
};

}}
