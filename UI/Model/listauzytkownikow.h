#pragma once

#include <QAbstractListModel>
#include <BL/bazadanych.h>

namespace UI { namespace Model {

class ListaUzytkownikow : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ListaUzytkownikow(BL::BazaDanych& bazaDanych, QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    BL::BazaDanych& m_BazaDanych;
};

}}
