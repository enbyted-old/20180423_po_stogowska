#include "mojapolka.h"
#include <BL/Model/film.h>
#include <algorithm>

namespace UI {
namespace Model {

MojaPolka::MojaPolka(std::shared_ptr<BL::Model::Uzytkownik> uzytkownik, QObject *parent)
    : QAbstractTableModel(parent)
    , m_uzytkownik(uzytkownik)
{
}

QVariant MojaPolka::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation != Qt::Orientation::Horizontal)
        return QVariant();

    switch (section) {
    case 0:
        return "Tytuł";
    case 1:
        return "Kod";
    case 2:
        return "Gatunek";
    case 3:
        return "Data premiery";
    case 4:
        return "Data wypożyczenia";
    case 5:
        return "Cena";
    default:
        return QVariant();
    }
}

bool MojaPolka::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {
        // FIXME: Implement me!
        emit headerDataChanged(orientation, section, section);
        return true;
    }
    return false;
}


int MojaPolka::rowCount(const QModelIndex &parent) const
{
    // Eliminuje ostrzeżenie o nieużywanym parametrze
    parent.isValid();
    return m_uzytkownik->Filmy().size();
}

int MojaPolka::columnCount(const QModelIndex &parent) const
{
    // Eliminuje ostrzeżenie o nieużywanym parametrze
    parent.isValid();
    return 6;
}

QVariant MojaPolka::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    std::shared_ptr<BL::Model::Film> film = m_uzytkownik->Filmy().at(index.row());
    if (! film)
        return QVariant();

    switch (index.column()) {
    case 0:
        return film->Tytul();
    case 1:
        return film->Kod();
    case 2:
        return film->Gatunek();
    case 3:
        return film->DataPremiery();
    case 4:
        return film->DataWypozyczenia();
    case 5:
        return film->Cena();
    default:
        return QVariant();
    }
}

bool MojaPolka::setData(const QModelIndex &idx, const QVariant &value, int role)
{
    emit dataChanged(index(0,0), index(rowCount(), columnCount()));
    if (data(idx, role) != value) {
        // FIXME: Implement me!
        //emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags MojaPolka::flags(const QModelIndex &index) const
{
    index.isValid(); // Eliminuje ostrzeżenie o nieużywanym argumencie

    return Qt::ItemIsSelectable | Qt::ItemIsEnabled; // FIXME: Implement me!
}

bool MojaPolka::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endInsertRows();
    return false;
}

} // namespace Model
} // namespace UI
