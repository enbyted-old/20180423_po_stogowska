#pragma once

#include <QAbstractListModel>
#include <BL/Model/uzytkownik.h>

namespace UI { namespace Model {

class ListaWiadomosciUzytkownika : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ListaWiadomosciUzytkownika(std::shared_ptr<BL::Model::Uzytkownik> uzytkownik, QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    std::shared_ptr<BL::Model::Uzytkownik> m_uzytkownik;
};

}}
