#include "listauzytkownikow.h"

namespace UI { namespace Model {

ListaUzytkownikow::ListaUzytkownikow(BL::BazaDanych& bazaDanych, QObject *parent)
    : QAbstractListModel(parent)
    , m_BazaDanych(bazaDanych)
{
}

QVariant ListaUzytkownikow::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation != Qt::Orientation::Horizontal)
        return QVariant();

    if (section != 0)
        return QVariant();

    return "Użytkownik";
}

int ListaUzytkownikow::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_BazaDanych.IloscUzytkownikow();
}

QVariant ListaUzytkownikow::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (!index.isValid())
        return QVariant();

    if (index.column() != 0)
        return QVariant();

    auto uzytkownik = m_BazaDanych.UzytkownikPoNumerzeWiersza(index.row());
    if (! uzytkownik)
        return QVariant();

    return uzytkownik->Imie() + " " + uzytkownik->Nazwisko() + "(" + uzytkownik->Login() + ")";
}

}}
