#include "listapracownikow.h"

namespace UI { namespace Model {

ListaPracownikow::ListaPracownikow(BL::BazaDanych& bazaDanych, QObject *parent)
    : QAbstractListModel(parent)
    , m_BazaDanych(bazaDanych)
{
}

QVariant ListaPracownikow::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation != Qt::Orientation::Horizontal)
        return QVariant();

    if (section != 0)
        return QVariant();

    return "Pracownik";
}

int ListaPracownikow::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_BazaDanych.IloscPracownikow();
}

QVariant ListaPracownikow::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (!index.isValid())
        return QVariant();

    if (index.column() != 0)
        return QVariant();

    auto pracownik = m_BazaDanych.PracownikPoNumerzeWiersza(index.row());
    if (! pracownik)
        return QVariant();

    return pracownik->Imie() + " " + pracownik->Nazwisko();
}

}}
