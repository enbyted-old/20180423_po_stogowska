#ifndef UI_MODEL_LISTAPRACOWNIKOW_H
#define UI_MODEL_LISTAPRACOWNIKOW_H

#include <QAbstractListModel>
#include <BL/bazadanych.h>

namespace UI {
namespace Model {

class ListaPracownikow : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ListaPracownikow(BL::BazaDanych& bazaDanych, QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    BL::BazaDanych& m_BazaDanych;
};

} // namespace Model
} // namespace UI

#endif // UI_MODEL_LISTAPRACOWNIKOW_H
