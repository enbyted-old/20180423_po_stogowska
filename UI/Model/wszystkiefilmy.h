#pragma once

#include <QAbstractTableModel>
#include <BL/bazadanych.h>

namespace UI { namespace Model {

class WszystkieFilmy : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit WszystkieFilmy(BL::BazaDanych& bazaDanych, QObject *parent = nullptr);

    // Nagłówki
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;


private:
    BL::BazaDanych& m_BazaDanych;
};

}}
