#pragma once

#include <QAbstractTableModel>
#include <BL/Model/uzytkownik.h>

namespace UI {
namespace Model {

class MojaPolka : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit MojaPolka(std::shared_ptr<BL::Model::Uzytkownik> uzytkownik, QObject *parent = nullptr);

    // Nagłówki
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

private:
    std::shared_ptr<BL::Model::Uzytkownik> m_uzytkownik;
};

} // namespace Model
} // namespace UI

