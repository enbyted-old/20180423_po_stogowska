#pragma once

#include <QAbstractTableModel>
#include <BL/Model/uzytkownik.h>

namespace UI { namespace Model {

class HistoriaUzytkownika : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit HistoriaUzytkownika(std::shared_ptr<BL::Model::Uzytkownik> uzytkownik, QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;

private:
    std::shared_ptr<BL::Model::Uzytkownik> m_uzytkownik;
};

}}
