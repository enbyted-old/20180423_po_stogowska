#include "wszystkiefilmy.h"
#include <algorithm>
#include <QRegularExpression>

namespace UI { namespace Model {

WszystkieFilmy::WszystkieFilmy(BL::BazaDanych& bazaDanych, QObject *parent)
    : QAbstractTableModel(parent)
    , m_BazaDanych(bazaDanych)
{
}

QVariant WszystkieFilmy::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation != Qt::Orientation::Horizontal)
        return QVariant();

    switch (section) {
    case 0:
        return "Tytuł";
    case 1:
        return "Gatunek";
    case 2:
        return "Data premiery";
    case 3:
        return "Status";
    case 4:
        return "Cena";
    default:
        return QVariant();
    }
}

bool WszystkieFilmy::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {
        // FIXME: Implement me!
        emit headerDataChanged(orientation, section, section);
        return true;
    }
    return false;
}


int WszystkieFilmy::rowCount(const QModelIndex &parent) const
{
    // Eliminuje ostrzeżenie o nieużywanym parametrze
    parent.isValid();
    return m_BazaDanych.IloscFilmow();
}

int WszystkieFilmy::columnCount(const QModelIndex &parent) const
{
    // Eliminuje ostrzeżenie o nieużywanym parametrze
    parent.isValid();
    return 5;
}

QVariant WszystkieFilmy::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    std::shared_ptr<BL::Model::Film> film = m_BazaDanych.FilmPoNumerzeWiersza(index.row());
    if (! film)
        return QVariant();

    switch (index.column()) {
    case 0:
        return film->Tytul();
    case 1:
        return film->Gatunek();
    case 2:
        return film->DataPremiery();
    case 3:
        return film->Wypozyczajacy() ? "Wypożyczony" : "W magazynie";
    case 4:
        return film->Cena();
    default:
        return QVariant();
    }
}

Qt::ItemFlags WszystkieFilmy::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsSelectable;

    return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}


} // namespace Model
} // namespace UI
