#include "historiauzytkownika.h"
#include <BL/Model/historia.h>
#include <BL/Model/film.h>

namespace UI {
namespace Model {

HistoriaUzytkownika::HistoriaUzytkownika(std::shared_ptr<BL::Model::Uzytkownik> uzytkownik, QObject *parent)
    : QAbstractTableModel(parent)
    , m_uzytkownik(uzytkownik)
{
}

QVariant HistoriaUzytkownika::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation != Qt::Orientation::Horizontal)
        return QVariant();

    switch (section) {
    case 0:
        return "Tytuł";
    case 1:
        return "Kod";
    case 2:
        return "Data wypożyczenia";
    case 3:
        return "Data zwrotu";
    default:
        return QVariant();
    }
}

int HistoriaUzytkownika::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_uzytkownik->Historia().size();
}

int HistoriaUzytkownika::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 4;
}

QVariant HistoriaUzytkownika::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    std::shared_ptr<BL::Model::Historia> row = m_uzytkownik->Historia().at(index.row());
    if (! row)
        return QVariant();

    switch (index.column()) {
    case 0:
        return row->Film()->Tytul();
    case 1:
        return row->Film()->Kod();
    case 2:
        return row->DataWypozyczenia();
    case 3:
        return row->DataZwrotu();
    default:
        return QVariant();
    }
}

Qt::ItemFlags HistoriaUzytkownika::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsSelectable;

    return Qt::ItemIsSelectable | Qt::ItemIsEnabled; // FIXME: Implement me!
}

} // namespace Model
} // namespace UI
