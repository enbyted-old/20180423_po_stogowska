#include "wybierztryb.h"
#include "ui_wybierztryb.h"

#include <QMessageBox>
#include <UI/Gosc/g_panel.h>
#include <UI/Pracownik/p_logowanie.h>
#include <UI/Pracownik/p_panel.h>
#include <UI/Uzytkownik/u_logowanie.h>
#include <UI/Uzytkownik/u_panel.h>
#include <BL/Model/pracownik.h>
#include <BL/Model/uzytkownik.h>

namespace UI {

WybierzTryb::WybierzTryb(BL::BazaDanych& bazaDanych, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WybierzTryb),
    m_BazaDanych(bazaDanych)
{
    ui->setupUi(this);
}

WybierzTryb::~WybierzTryb()
{
    delete ui;
}

} // namespace UI

void UI::WybierzTryb::on_btnGosc_clicked()
{
    hide();
    Gosc::Panel(m_BazaDanych).exec();
    show();
}

void UI::WybierzTryb::on_btnUzytkownik_clicked()
{
    hide();
    Uzytkownik::Logowanie logowanie;
    if (logowanie.exec() == Uzytkownik::Logowanie::Accepted)
    {
        if (auto uzytkownik = m_BazaDanych.ZalogujUzytkownika(logowanie.Login(), logowanie.Haslo()))
        {
            Uzytkownik::Panel(m_BazaDanych, uzytkownik).exec();
        }
        else
        {
            QMessageBox(QMessageBox::Critical, "Błąd", "Nieprawidłowy login lub hasło!").exec();
        }

    }
    show();
}

void UI::WybierzTryb::on_btnPracownik_clicked()
{
    hide();
    Pracownik::Logowanie logowanie;
    if (logowanie.exec() == Pracownik::Logowanie::Accepted)
    {
        if (auto pracownik = m_BazaDanych.ZalogujPracownika(logowanie.Login(), logowanie.Haslo()))
        {
            Pracownik::Panel(m_BazaDanych, pracownik).exec();
        }
        else
        {
            QMessageBox(QMessageBox::Critical, "Błąd", "Nieprawidłowy login lub hasło!").exec();
        }

    }
    show();
}
