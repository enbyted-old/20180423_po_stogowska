#include "p_panel.h"
#include "ui_p_panel.h"
#include <UI/nowawiadomosc.h>
#include <UI/Pracownik/p_edytujfilm.h>
#include <UI/Model/wszystkiefilmy.h>
#include <UI/Model/listawiadomoscipracownika.h>
#include <UI/Model/mojapolka.h>
#include <UI/Model/historiauzytkownika.h>
#include <UI/Model/listapracownikow.h>
#include <QMessageBox>
#include "p_wybierzuzytkownika.h"
#include <cmath>

namespace UI { namespace Pracownik {

Panel::Panel(BL::BazaDanych& bazaDanych, std::shared_ptr<BL::Model::Pracownik> pracownik, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Panel),
    m_BazaDanych(bazaDanych),
    m_Pracownik(pracownik)
{
    ui->setupUi(this);
    RefreshViews();
    ui->wiadomosciWybrana->hide();
}

Panel::~Panel()
{
    delete ui;
}

void Panel::RefreshViews()
{
    bool administrator = m_Pracownik->Uprawnienia() >= DA::Model::Pracownik::PoziomUprawnien::ADMINISTRATOR;

    ui->KatalogFilmow->setModel(new UI::Model::WszystkieFilmy(m_BazaDanych));
    ui->wiadomosciLista->setModel(new UI::Model::ListaWiadomosciPracownika(m_Pracownik));
    ui->pracownicyWybierz->setModel(new UI::Model::ListaPracownikow(m_BazaDanych));

    ui->tabPracownicy->setEnabled(administrator);

    if (! m_WybranyUzytkownik)
    {
        ui->uzytkownicyInformacje->hide();
        ui->uzytkownicyNazwa->setText("Nie wybrano użytkownika");
    }
    else
    {
        ui->uzytkownicyInformacje->show();
        ui->uzytkownicyNazwa->setText(m_WybranyUzytkownik->ImieNazwisko());
        ui->uzytkownicHistoria->setModel(new UI::Model::HistoriaUzytkownika(m_WybranyUzytkownik));
        ui->uzytkownicyWypozyczenia->setModel(new UI::Model::MojaPolka(m_WybranyUzytkownik));
        ui->uzytkownicyZwroc->setEnabled(ui->uzytkownicyWypozyczenia->selectionModel()->hasSelection());
        ui->uzytkownicyUsun->setEnabled(administrator);
    }
}

void Panel::on_KatalogFilmow_clicked(const QModelIndex &index)
{
    auto film = m_BazaDanych.FilmPoNumerzeWiersza(index.row());
    if (! film)
        return;

    ui->katalogObsada->setText("Obsada: " + film->Obsada());
    ui->katalogOpis->setText(film->Opis());
    ui->katalogRezyser->setText("Reżyser: " + film->Rezyser());

    // Zaokrąglij do 2 miejsc po przecinku
    float ocena = std::round(film->Ocena() * 100) / 100.0f;
    if (ocena > 0)
        ui->katalogOcena->setText(QString("Ocena: ") + QString::number(ocena) + "/10");
    else
        ui->katalogOcena->setText("Brak ocen");

    ui->katalogEdytuj->setEnabled(true);

    if (film->Wypozyczajacy())
    {
        ui->katalogWypozyczony->setText("Wypożyczony przez " + film->Wypozyczajacy()->Imie() + " " + film->Wypozyczajacy()->Nazwisko());
        ui->katalogWypozycz->setEnabled(false);
        ui->katalogZwroc->setEnabled(true);
    }
    else
    {
        ui->katalogWypozyczony->setText("W magazynie");
        ui->katalogWypozycz->setEnabled(true);
        ui->katalogZwroc->setEnabled(false);
    }
}

void Panel::on_katalogWypozycz_clicked()
{
    if (! ui->KatalogFilmow->selectionModel()->hasSelection())
        return;

    int row = ui->KatalogFilmow->selectionModel()->selectedRows().at(0).row();
    auto film = m_BazaDanych.FilmPoNumerzeWiersza(row);

    if (! film)
        return;

    WybierzUzytkownika dialog(m_BazaDanych);
    if (dialog.exec() == QDialog::Rejected)
        return;

    if (! dialog.WybranyUzytkownik())
        return;

    try
    {
        film->Wypozycz(dialog.WybranyUzytkownik());
        m_BazaDanych.Zapisz();
        RefreshViews();
    }
    catch (std::exception& ex)
    {
        QMessageBox(QMessageBox::Critical, "Błąd", ex.what(), QMessageBox::Ok, this).exec();
    }
}

void Panel::on_katalogZwroc_clicked()
{
    if (! ui->KatalogFilmow->selectionModel()->hasSelection())
        return;

    int row = ui->KatalogFilmow->selectionModel()->selectedRows().at(0).row();
    auto film = m_BazaDanych.FilmPoNumerzeWiersza(row);

    if (! film)
        return;

    try
    {
        film->Zwroc();
        m_BazaDanych.Zapisz();
        RefreshViews();
    }
    catch (std::exception& ex)
    {
        QMessageBox(QMessageBox::Critical, "Błąd", ex.what(), QMessageBox::Ok, this).exec();
    }
}

void Panel::on_katalogDodaj_clicked()
{
    EdytujFilm dialog;
    dialog.Nowy();
    if (dialog.exec() == QDialog::Accepted)
    {
        try
        {
            m_BazaDanych.DodajFilm(
                        dialog.Tytul(),
                        dialog.Kod(),
                        dialog.Gatunek(),
                        dialog.DataPremiery(),
                        dialog.Rezyser(),
                        dialog.Obsada(),
                        dialog.Opis(),
                        dialog.Cena());
            m_BazaDanych.Zapisz();
            RefreshViews();
        }
        catch (std::exception& ex)
        {
            QMessageBox(QMessageBox::Critical, "Błąd", ex.what(), QMessageBox::Ok, this).exec();
        }
    }
}


void Panel::on_katalogEdytuj_clicked()
{
    if (! ui->KatalogFilmow->selectionModel()->hasSelection())
        return;

    int row = ui->KatalogFilmow->selectionModel()->selectedRows().at(0).row();
    auto film = m_BazaDanych.FilmPoNumerzeWiersza(row);

    if (! film)
        return;

    EdytujFilm dialog;
    dialog.Edytuj();
    dialog.Tytul(film->Tytul());
    dialog.Kod(film->Kod());
    dialog.Gatunek(film->Gatunek());
    dialog.DataPremiery(film->DataPremiery());
    dialog.Rezyser(film->Rezyser());
    dialog.Obsada(film->Obsada());
    dialog.Opis(film->Opis());
    dialog.Cena(film->Cena());

    if (dialog.exec() == QDialog::Accepted)
    {
        try
        {

            film->Tytul(dialog.Tytul());
            film->Kod(dialog.Kod());
            film->Gatunek(dialog.Gatunek());
            film->DataPremiery(dialog.DataPremiery());
            film->Rezyser(dialog.Rezyser());
            film->Obsada(dialog.Obsada());
            film->Opis(dialog.Opis());
            film->Cena(dialog.Cena());

            m_BazaDanych.Zapisz();
            RefreshViews();
        }
        catch (std::exception& ex)
        {
            QMessageBox(QMessageBox::Critical, "Błąd", ex.what(), QMessageBox::Ok, this).exec();
        }
    }
}

void Panel::on_wiadomosciNowa_clicked()
{
    NowaWiadomosc dialog(m_BazaDanych);
    dialog.Uzytkownik(std::shared_ptr<BL::Model::Uzytkownik>());
    if (dialog.exec() != QDialog::Accepted)
        return;

    m_BazaDanych.WyslijWiadomosc(dialog.Uzytkownik(), m_Pracownik, DA::Model::Wiadomosc::DO_UZYTKOWNIKA, dialog.Temat(), dialog.Tresc());
    m_BazaDanych.Zapisz();
    RefreshViews();
}

void Panel::on_wiadomosciLista_clicked(const QModelIndex &index)
{
    auto wiadomosc = m_Pracownik->Wiadomosci().at(index.row());
    if (! wiadomosc)
    {
        ui->wiadomosciWybrana->hide();
        ui->wiadomosciOdpowiedz->setEnabled(false);
        return;
    }

    ui->wiadomosciWybrana->show();
    ui->wiadomosciOdpowiedz->setEnabled(true);
    ui->wiadomosciTemat->setText(wiadomosc->Temat());
    ui->wiadomosciTresc->setPlainText(wiadomosc->Tresc());

    if (wiadomosc->Kierunek() == DA::Model::Wiadomosc::DO_PRACOWNIKA)
        ui->wiadomoscOdDo->setText("Od: " + wiadomosc->Uzytkownik()->ImieNazwisko());
    else
        ui->wiadomoscOdDo->setText("Do: " + wiadomosc->Uzytkownik()->ImieNazwisko());
}

void Panel::on_wiadomosciOdpowiedz_clicked()
{
    auto wiadomosc = m_Pracownik->Wiadomosci().at(ui->wiadomosciLista->currentIndex().row());

    NowaWiadomosc dialog(m_BazaDanych);
    dialog.Uzytkownik(wiadomosc->Uzytkownik());
    dialog.Temat("RE: " + wiadomosc->Temat());

    if (dialog.exec() != QDialog::Accepted)
        return;

    m_BazaDanych.WyslijWiadomosc(dialog.Uzytkownik(), m_Pracownik, DA::Model::Wiadomosc::DO_UZYTKOWNIKA, dialog.Temat(), dialog.Tresc());
    m_BazaDanych.Zapisz();
    RefreshViews();
}

void Panel::on_uzytkownicyWybierz_clicked()
{
    WybierzUzytkownika dialog(m_BazaDanych);
    if (dialog.exec() == QDialog::Rejected)
        return;

    m_WybranyUzytkownik = dialog.WybranyUzytkownik();
    RefreshViews();
}

void Panel::on_uzytkownicyUsun_clicked()
{
    if (! m_WybranyUzytkownik)
        return;

    if (m_WybranyUzytkownik->Filmy().size() > 0)
    {
        QMessageBox::critical(this, "Błąd", "Nie można usunąć użytkownika, który ma wypożyczone filmy");
    }

    m_BazaDanych.UsunUzytkownika(m_WybranyUzytkownik);
    m_BazaDanych.Zapisz();
    m_WybranyUzytkownik.reset();
    RefreshViews();
}

void Panel::on_uzytkownicyZwroc_clicked()
{
    if (! ui->uzytkownicyWypozyczenia->selectionModel()->hasSelection())
        return;

    int row = ui->uzytkownicyWypozyczenia->selectionModel()->selectedRows().at(0).row();
    auto film = m_WybranyUzytkownik->Filmy().at(row);

    if (! film)
        return;

    try
    {
        film->Zwroc();
        m_BazaDanych.Zapisz();
        RefreshViews();
    }
    catch (std::exception& ex)
    {
        QMessageBox(QMessageBox::Critical, "Błąd", ex.what(), QMessageBox::Ok, this).exec();
    }
}

void Panel::on_uzytkownicyWypozyczenia_clicked(const QModelIndex &index)
{
    index.isValid(); // Eliminuje ostrzeżenie o nieużywanym argumencie
    ui->uzytkownicyZwroc->setEnabled(ui->uzytkownicyWypozyczenia->selectionModel()->hasSelection());
}

void Panel::on_uzytkownicyWypozyczenia_doubleClicked(const QModelIndex &index)
{
    index.isValid(); // Eliminuje ostrzeżenie o nieużywanym argumencie
    on_uzytkownicyZwroc_clicked();
}

void Panel::on_pracownicyWybierz_currentIndexChanged(int index)
{
    auto pracownik = m_BazaDanych.PracownikPoNumerzeWiersza(index);
    if (pracownik) // Wstawienie aktualnych danych pracownika z bazy
    {
        ui->pracownicyEdytujImie->setText(pracownik->Imie());
        ui->pracownicyEdytujImie->setEnabled(true);

        ui->pracownicyEdytujNazwisko->setText(pracownik->Nazwisko());
        ui->pracownicyEdytujNazwisko->setEnabled(true);

        ui->pracownicyEdytujLogin->setText(pracownik->Login());
        ui->pracownicyEdytujLogin->setEnabled(true);

        ui->pracownicyEdytujHaslo->setText("");
        ui->pracownicyEdytujHaslo->setEnabled(true);

        ui->pracownicyEdytujAdministrator->setChecked(pracownik->Uprawnienia() == DA::Model::Pracownik::PoziomUprawnien::ADMINISTRATOR);
        ui->pracownicyEdytujAdministrator->setEnabled(true);

        ui->pracownicyEdytujResetuj->setEnabled(true);
        ui->pracownicyEdytujUsun->setEnabled(true);
        ui->pracownicyEdytujZapisz->setEnabled(true);
    }
    else // Nie jest wybrany żaden pracownik - zablokuj formularz (nie powinno się zdarzyć)
    {
        ui->pracownicyEdytujImie->setText("");
        ui->pracownicyEdytujImie->setEnabled(false);

        ui->pracownicyEdytujNazwisko->setText("");
        ui->pracownicyEdytujNazwisko->setEnabled(false);

        ui->pracownicyEdytujLogin->setText("");
        ui->pracownicyEdytujLogin->setEnabled(false);

        ui->pracownicyEdytujHaslo->setText("");
        ui->pracownicyEdytujHaslo->setEnabled(false);

        ui->pracownicyEdytujAdministrator->setChecked(false);
        ui->pracownicyEdytujAdministrator->setEnabled(false);

        ui->pracownicyEdytujResetuj->setEnabled(false);
        ui->pracownicyEdytujUsun->setEnabled(false);
        ui->pracownicyEdytujZapisz->setEnabled(false);

    }
}

void Panel::on_pracownicyEdytujResetuj_clicked()
{
    on_pracownicyWybierz_currentIndexChanged(ui->pracownicyWybierz->currentIndex());
}

void Panel::on_pracownicyEdytujZapisz_clicked()
{
    auto pracownik = m_BazaDanych.PracownikPoNumerzeWiersza(ui->pracownicyWybierz->currentIndex());
    bool hasloZmienione = false;

    if (! pracownik)
        return;

    if (m_Pracownik->Id() == pracownik->Id())
    {
        if (ui->pracownicyEdytujAdministrator->checkState() != Qt::Checked)
        {
            QMessageBox::warning(this, "Uwaga", "Nie możesz odebrać praw administratora samemu sobie!");
            return;
        }
    }

    auto pracownikPoNowymLoginie = m_BazaDanych.PracownikPoLoginie(ui->pracownicyEdytujLogin->text());

    if (pracownikPoNowymLoginie && pracownikPoNowymLoginie->Id() != pracownik->Id())
    {
        QMessageBox::warning(this, "Błąd", "Pracownik o takim loginie już istnieje!");
        return;
    }

    pracownik->Imie(ui->pracownicyEdytujImie->text());
    pracownik->Nazwisko(ui->pracownicyEdytujNazwisko->text());
    pracownik->Login(ui->pracownicyEdytujLogin->text());


    if (ui->pracownicyEdytujHaslo->text().trimmed() != "")
    {
        hasloZmienione = true;
        pracownik->ZmienHaslo(ui->pracownicyEdytujHaslo->text());
    }

    if (ui->pracownicyEdytujAdministrator->checkState() == Qt::Checked)
        pracownik->Uprawnienia(DA::Model::Pracownik::PoziomUprawnien::ADMINISTRATOR);
    else
        pracownik->Uprawnienia(DA::Model::Pracownik::PoziomUprawnien::OBSLUGA_KLIENTA);

    m_BazaDanych.Zapisz();
    RefreshViews();

    if (hasloZmienione)
    {
        ui->pracownicyEdytujHaslo->setText("");
        QMessageBox::information(this, "Edycja pracownika", "Hasło zostało zmienione");
    }
}

void Panel::on_pracownicyEdytujUsun_clicked()
{
    auto pracownik = m_BazaDanych.PracownikPoNumerzeWiersza(ui->pracownicyWybierz->currentIndex());

    if (! pracownik)
     return;

    if (m_Pracownik->Id() == pracownik->Id())
    {
        QMessageBox::warning(this, "Uwaga", "Nie możesz usunąć samego siebie!");
        return;
    }

    if (QMessageBox::Yes != QMessageBox::question(this, "Usuwanie pracownika", "Czy na pewno chcesz usunąć tego pracownika i wszystkie skojarzone z nim dane?"))
    {
        return;
    }

    m_BazaDanych.UsunPracownika(pracownik);
    m_BazaDanych.Zapisz();
    RefreshViews();
}

void Panel::on_pracownicyNowyDodaj_clicked()
{
    QString imie = ui->pracownicyNowyImie->text();
    QString nazwisko = ui->pracownicyNowyNazwisko->text();
    QString login = ui->pracownicyNowyLogin->text();
    QString haslo = ui->pracownicyNowyHaslo->text();
    bool administrator = ui->pracownicyNowyAdministrator->checkState() == Qt::Checked;

    try
    {
        auto uprawnienia = administrator ? DA::Model::Pracownik::PoziomUprawnien::ADMINISTRATOR : DA::Model::Pracownik::PoziomUprawnien::OBSLUGA_KLIENTA;
        m_BazaDanych.DodajPracownika(login, haslo, imie, nazwisko, uprawnienia);
        m_BazaDanych.Zapisz();
        RefreshViews();
        ui->pracownicyNowyImie->setText("");
        ui->pracownicyNowyNazwisko->setText("");
        ui->pracownicyNowyLogin->setText("");
        ui->pracownicyNowyHaslo->setText("");
        ui->pracownicyNowyAdministrator->setChecked(false);
    }
    catch(std::exception& ex)
    {
        QMessageBox::critical(this, "Błąd", ex.what());
    }
}

}}
