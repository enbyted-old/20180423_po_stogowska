#include "p_edytujfilm.h"
#include "ui_p_edytujfilm.h"
#include <QMessageBox>

namespace UI { namespace Pracownik {

EdytujFilm::EdytujFilm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EdytujFilm)
{
    ui->setupUi(this);
}

EdytujFilm::~EdytujFilm()
{
    delete ui;
}

void EdytujFilm::on_buttonBox_accepted()
{
    if (Tytul().trimmed() == "")
    {
        QMessageBox::critical(this, "Błąd", "Tytuł nie może być pusty!");
        return;
    }

    if (Kod().trimmed() == "")
    {
        QMessageBox::critical(this, "Błąd", "Kod nie może być pusty!");
        return;
    }

    accept();
}

void EdytujFilm::Edytuj()
{
    ui->naglowek->setText("Edytuj film");
}

void EdytujFilm::Nowy()
{
    ui->naglowek->setText("Nowy film");
}

QString EdytujFilm::Tytul()
{
    return ui->tytul->text();
}

void EdytujFilm::Tytul(const QString &tytul)
{
    ui->tytul->setText(tytul);
}

QString EdytujFilm::Kod()
{
    return ui->kod->text();
}

void EdytujFilm::Kod(const QString &kod)
{
    ui->kod->setText(kod);
}

QString EdytujFilm::Gatunek()
{
    return ui->gatunek->text();
}

void EdytujFilm::Gatunek(const QString &gatunek)
{
    ui->gatunek->setText(gatunek);
}

QString EdytujFilm::DataPremiery()
{
    return ui->dataPremiery->text();
}

void EdytujFilm::DataPremiery(const QString &dataPremiery)
{
    ui->dataPremiery->setText(dataPremiery);
}

QString EdytujFilm::Rezyser()
{
    return ui->rezyser->text();
}

void EdytujFilm::Rezyser(const QString &rezyser)
{
    ui->rezyser->setText(rezyser);
}

QString EdytujFilm::Obsada()
{
    return ui->obsada->text();
}

void EdytujFilm::Obsada(const QString &obsada)
{
    ui->obsada->setText(obsada);
}

QString EdytujFilm::Opis()
{
    return ui->opis->text();
}

void EdytujFilm::Opis(const QString &opis)
{
    ui->opis->setText(opis);
}

float EdytujFilm::Cena()
{
    return ui->cena->value();
}

void EdytujFilm::Cena(float cena)
{
    ui->cena->setValue(cena);
}

}}

