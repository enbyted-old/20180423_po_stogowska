#include "p_wybierzuzytkownika.h"
#include "ui_p_wybierzuzytkownika.h"
#include <UI/Model/listauzytkownikow.h>

namespace UI { namespace Pracownik {

WybierzUzytkownika::WybierzUzytkownika(BL::BazaDanych& bazaDanych, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WybierzUzytkownika),
    m_BazaDanych(bazaDanych)
{
    ui->setupUi(this);
    ui->listaUzytkownikow->setModel(new Model::ListaUzytkownikow(m_BazaDanych));
}

WybierzUzytkownika::~WybierzUzytkownika()
{
    delete ui;
}

std::shared_ptr<BL::Model::Uzytkownik> WybierzUzytkownika::WybranyUzytkownik()
{
    if (! ui->listaUzytkownikow->selectionModel()->hasSelection())
        return std::shared_ptr<BL::Model::Uzytkownik>();

    int wiersz = ui->listaUzytkownikow->selectionModel()->selectedIndexes()[0].row();
    auto uzytkownik = m_BazaDanych.UzytkownikPoNumerzeWiersza(wiersz);

    return uzytkownik;
}

void WybierzUzytkownika::on_listaUzytkownikow_doubleClicked(const QModelIndex &index)
{
    int wiersz = index.row();
    auto uzytkownik = m_BazaDanych.UzytkownikPoNumerzeWiersza(wiersz);

    if (! uzytkownik)
        return;

    accept();
}

}}
