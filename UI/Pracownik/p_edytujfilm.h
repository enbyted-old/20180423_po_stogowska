#ifndef UI_PRACOWNIK_P_EDYTUJFILM_H
#define UI_PRACOWNIK_P_EDYTUJFILM_H

#include <QDialog>

namespace UI {
namespace Pracownik {

namespace Ui {
class EdytujFilm;
}

class EdytujFilm : public QDialog
{
    Q_OBJECT

public:
    explicit EdytujFilm(QWidget *parent = 0);
    ~EdytujFilm();

private slots:
    void on_buttonBox_accepted();

public:
    void Edytuj();
    void Nowy();

    QString Tytul();
    void Tytul(const QString& tytul);

    QString Kod();
    void Kod(const QString& kod);

    QString Gatunek();
    void Gatunek(const QString& gatunek);

    QString DataPremiery();
    void DataPremiery(const QString& dataPremiery);

    QString Rezyser();
    void Rezyser(const QString& rezyser);

    QString Obsada();
    void Obsada(const QString& obsada);

    QString Opis();
    void Opis(const QString& opis);

    float Cena();
    void Cena(float cena);

private:
    Ui::EdytujFilm *ui;
};


} // namespace Pracownik
} // namespace UI
#endif // UI_PRACOWNIK_P_EDYTUJFILM_H
