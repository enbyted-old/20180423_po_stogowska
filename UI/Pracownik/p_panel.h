#pragma once

#include <QDialog>
#include <BL/Model/pracownik.h>
#include <BL/Model/uzytkownik.h>
#include <BL/bazadanych.h>
#include <memory>

namespace UI {
namespace Pracownik {

namespace Ui {
class Panel;
}

class Panel : public QDialog
{
    Q_OBJECT

public:
    explicit Panel(BL::BazaDanych& bazaDanych, std::shared_ptr<BL::Model::Pracownik> pracownik, QWidget *parent = 0);
    ~Panel();
private slots:
    void on_KatalogFilmow_clicked(const QModelIndex &index);

    void on_katalogWypozycz_clicked();

    void on_katalogZwroc_clicked();

    void on_wiadomosciNowa_clicked();

    void on_wiadomosciLista_clicked(const QModelIndex &index);

    void on_wiadomosciOdpowiedz_clicked();

    void on_uzytkownicyWybierz_clicked();

    void on_uzytkownicyUsun_clicked();

    void on_uzytkownicyZwroc_clicked();

    void on_uzytkownicyWypozyczenia_clicked(const QModelIndex &index);

    void on_uzytkownicyWypozyczenia_doubleClicked(const QModelIndex &index);

    void on_pracownicyWybierz_currentIndexChanged(int index);

    void on_pracownicyEdytujResetuj_clicked();

    void on_pracownicyEdytujZapisz_clicked();

    void on_pracownicyEdytujUsun_clicked();

    void on_pracownicyNowyDodaj_clicked();

    void on_katalogDodaj_clicked();

    void on_katalogEdytuj_clicked();

private:
    void RefreshViews();
private:
    Ui::Panel *ui;
    BL::BazaDanych& m_BazaDanych;
    std::shared_ptr<BL::Model::Pracownik> m_Pracownik;
    std::shared_ptr<BL::Model::Uzytkownik> m_WybranyUzytkownik;
};


} // namespace Pracownik
} // namespace UI

