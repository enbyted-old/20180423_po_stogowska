#include "p_logowanie.h"
#include "ui_p_logowanie.h"

namespace UI {
namespace Pracownik {

Logowanie::Logowanie(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Logowanie)
{
    ui->setupUi(this);
}

Logowanie::~Logowanie()
{
    delete ui;
}

QString Logowanie::Login() const
{
    return ui->txtLogin->text();
}

QString Logowanie::Haslo() const
{
    return ui->txtHaslo->text();
}

} // namespace Pracownik
} // namespace UI
