#pragma once

#include <QDialog>
#include <BL/bazadanych.h>

namespace UI {
namespace Pracownik {

namespace Ui {
class WybierzUzytkownika;
}

class WybierzUzytkownika : public QDialog
{
    Q_OBJECT

public:
    explicit WybierzUzytkownika(BL::BazaDanych& bazaDanych, QWidget *parent = 0);
    ~WybierzUzytkownika();

    std::shared_ptr<BL::Model::Uzytkownik> WybranyUzytkownik();

private slots:
    void on_listaUzytkownikow_doubleClicked(const QModelIndex &index);

private:
    Ui::WybierzUzytkownika *ui;
    BL::BazaDanych& m_BazaDanych;
};


}}
