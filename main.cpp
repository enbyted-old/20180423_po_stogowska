#include "mainwindow.h"
#include <UI/wybierztryb.h>
#include <BL/bazadanych.h>
#include <QApplication>
#include <QMessageBox>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    BL::BazaDanych bazaDanych("Dane");
    try
    {
        bazaDanych.Wczytaj();
    }
    catch (std::exception& ex)
    {
        QMessageBox::critical(nullptr, "Błąd", QString("Błąd podczas wczytywania bazy danych!\n") + ex.what());
        return 1;
    }

    UI::WybierzTryb oknoGlowne(bazaDanych);
    oknoGlowne.show();

    return a.exec();
}
