#-------------------------------------------------
#
# Project created by QtCreator 2018-04-23T21:47:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Biblioteka
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += object_parallel_to_source
CONFIG+= static

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
        DA/Model/uzytkownik.cpp \
        DA/Model/pracownik.cpp \
        DA/bazadanych.cpp \
        BL/bazadanych.cpp \
        BL/Model/uzytkownik.cpp \
        BL/Model/pracownik.cpp \
    UI/wybierztryb.cpp \
    DA/Model/logujacy.cpp \
    UI/Pracownik/p_logowanie.cpp \
    UI/Pracownik/p_panel.cpp \
    UI/Uzytkownik/u_logowanie.cpp \
    UI/Uzytkownik/u_panel.cpp \
    UI/Gosc/g_panel.cpp \
    DA/Model/film.cpp \
    BL/Model/film.cpp \
    UI/Model/film.cpp \
    UI/Gosc/g_register.cpp \
    UI/Model/mojapolka.cpp \
    DA/Model/historia.cpp \
    BL/Model/historia.cpp \
    UI/Model/historiauzytkownika.cpp \
    UI/Model/wszystkiefilmy.cpp \
    UI/Pracownik/p_wybierzuzytkownika.cpp \
    UI/Model/listauzytkownikow.cpp \
    DA/Model/wiadomosc.cpp \
    BL/Model/wiadomosc.cpp \
    UI/Model/listawiadomosciuzytkownika.cpp \
    UI/Model/listapracownikow.cpp \
    UI/nowawiadomosc.cpp \
    UI/Model/listawiadomoscipracownika.cpp \
    DA/Model/recenzja.cpp \
    BL/Model/recenzja.cpp \
    UI/Uzytkownik/u_zwrocfilm.cpp \
    UI/Pracownik/p_edytujfilm.cpp

HEADERS += \
        mainwindow.h \
        DA/Model/uzytkownik.h \
        DA/Model/pracownik.h \
        DA/bazadanych.h \
        BL/bazadanych.h \
        BL/Model/pracownik.h \
        BL/Model/uzytkownik.h \
    UI/wybierztryb.h \
    DA/Model/logujacy.h \
    UI/Pracownik/p_logowanie.h \
    UI/Pracownik/p_panel.h \
    UI/Uzytkownik/u_logowanie.h \
    UI/Uzytkownik/u_panel.h \
    UI/Gosc/g_panel.h \
    DA/Model/film.h \
    BL/Model/film.h \
    DA/Model/model.h \
    UI/Model/film.h \
    UI/Gosc/g_register.h \
    UI/Model/mojapolka.h \
    DA/Model/historia.h \
    BL/Model/historia.h \
    UI/Model/historiauzytkownika.h \
    UI/Model/wszystkiefilmy.h \
    UI/Pracownik/p_wybierzuzytkownika.h \
    UI/Model/listauzytkownikow.h \
    DA/Model/wiadomosc.h \
    BL/Model/wiadomosc.h \
    UI/Model/listawiadomosciuzytkownika.h \
    UI/Model/listapracownikow.h \
    UI/nowawiadomosc.h \
    UI/Model/listawiadomoscipracownika.h \
    DA/Model/recenzja.h \
    BL/Model/recenzja.h \
    UI/Uzytkownik/u_zwrocfilm.h \
    UI/Pracownik/p_edytujfilm.h

FORMS += \
        mainwindow.ui \
    UI/wybierztryb.ui \
    UI/Uzytkownik/u_logowanie.ui \
    UI/Pracownik/p_logowanie.ui \
    UI/Gosc/g_panel.ui \
    UI/Pracownik/p_panel.ui \
    UI/Uzytkownik/u_panel.ui \
    UI/Gosc/g_register.ui \
    UI/Pracownik/p_wybierzuzytkownika.ui \
    UI/nowawiadomosc.ui \
    UI/Uzytkownik/u_zwrocfilm.ui \
    UI/Pracownik/p_edytujfilm.ui
