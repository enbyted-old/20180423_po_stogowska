#pragma once

#include <QString>
#include <DA/Model/model.h>
#include <DA/Model/logujacy.h>

namespace DA { namespace Model {

    class Uzytkownik : public Logujacy, public Model
    {
    public:
        int id;
        QString imie;
        QString nazwisko;

    public:
        virtual ~Uzytkownik() = default;
        Uzytkownik() = default;
        Uzytkownik(const QString& dane);

        virtual QString Serializuj() const;
    };
}}
