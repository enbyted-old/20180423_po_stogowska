﻿#pragma once

#include <QString>
#include <DA/Model/model.h>
#include <DA/Model/logujacy.h>

namespace DA { namespace Model {

    class Pracownik : public Logujacy, public Model
    {
    public:
        enum class PoziomUprawnien
        {
            ZABLOKOWANY = 0,
            OBSLUGA_KLIENTA = 1,
            ADMINISTRATOR = 10,
        };
        int id;
        QString imie;
        QString nazwisko;
        PoziomUprawnien uprawnienia;

    public:
        virtual ~Pracownik() = default;
        Pracownik() = default;
        Pracownik(const QString& dane);

        virtual QString Serializuj() const;
    };

}}
