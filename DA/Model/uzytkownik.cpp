#include "uzytkownik.h"

#include <QStringList>
#include <QTextStream>
#include <stdexcept>

namespace DA { namespace Model {

Uzytkownik::Uzytkownik(const QString &dane)
{
    // Rozbija "dane" na tablicę kolejnych fragmentów między ';'
    QStringList czesci = dane.split(';');
    if (czesci.length() < 4)
        throw std::runtime_error("Nieprawidlowy rekord uzytkownika");

    this->id = czesci[0].toInt();
    this->login = czesci[1];
    this->hashHasla = czesci[2];
    this->imie = czesci[3];
    this->nazwisko = czesci[4];
}

QString Uzytkownik::Serializuj() const
{
    QString dane;
    QTextStream str(&dane);

    str << id << ';';
    str << login << ';';
    str << hashHasla << ';';
    str << imie << ';';
    str << nazwisko;

    return dane;
}

}}
