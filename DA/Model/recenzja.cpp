#include "recenzja.h"

#include <QStringList>
#include <QTextStream>
#include <stdexcept>

namespace DA { namespace Model {

Recenzja::Recenzja(const QString &dane)
{
    // Rozbija "dane" na tablicę kolejnych fragmentów między ';'
    QStringList czesci = dane.split(';');
    if (czesci.length() < 4)
        throw std::runtime_error("Nieprawidlowy rekord recenzji");

    this->id = czesci[0].toInt();
    this->idUzytkownika = czesci[1].toInt();
    this->idFilmu = czesci[2].toInt();
    this->ocena = czesci[3].toInt();
    // Zamiana bezpiecznego ciągu na oryginalny tekst
    this->tresc = czesci[4].replace("\\n", "\n").replace("\\:", ";").replace("\\\\", "\\");
}

QString Recenzja::Serializuj() const
{
    QString dane;
    QTextStream str(&dane);

    str << id << ';';
    str << idUzytkownika << ';';
    str << idFilmu << ';';
    str << ocena << ';';
    // Zamiana niebezpiecznych znaków na bezpieczny ciąg
    str << QString(tresc).replace("\r", "").replace("\\", "\\").replace("\n", "\\n").replace(";", "\\:");

    return dane;
}

}}
