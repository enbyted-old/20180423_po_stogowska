#include "logujacy.h"

#include <QCryptographicHash>

namespace DA {
namespace Model {

Logujacy::Logujacy()
{

}

bool Logujacy::SprawdzHaslo(const QString &haslo) const
{
    return Hash(haslo) == this->hashHasla;
}

void Logujacy::ZmienHaslo(const QString &noweHaslo)
{
    this->hashHasla = Hash(noweHaslo);
}

QString Logujacy::Hash(const QString &tekst) const
{
    QCryptographicHash hash(QCryptographicHash::Algorithm::Sha512);
    hash.addData(tekst.toUtf8());
    return hash.result().toHex();
}

} // namespace Model
} // namespace DA
