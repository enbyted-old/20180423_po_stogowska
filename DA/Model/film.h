#pragma once

#include <QString>
#include <DA/Model/model.h>

namespace DA { namespace Model {

class Film : public Model
{
public:
    int id;
    QString kod;
    QString tytul;
    QString opis;
    QString rezyser;
    QString gatunek;
    QString dataPremiery;
    QString obsada;
    float cena;
    int idWypozyczajacego;
    QString dataWypozyczenia;
public:
    virtual ~Film() = default;
    Film() = default;
    Film(const QString& dane);

    virtual QString Serializuj() const;
};

}}
