#pragma once

#include <QString>
#include <DA/Model/model.h>

namespace DA { namespace Model {

class Recenzja : public Model
{
public:
    int id;
    int idUzytkownika;
    int idFilmu;
    int ocena;
    QString tresc;
public:
    virtual ~Recenzja() noexcept = default;
    Recenzja() = default;
    Recenzja(const QString& dane);

    virtual QString Serializuj() const;
};

}}
