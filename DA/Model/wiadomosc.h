#pragma once

#include <QString>
#include <DA/Model/model.h>

namespace DA { namespace Model {

class Wiadomosc : public Model
{
public:
    enum Kierunek
    {
        DO_PRACOWNIKA,
        DO_UZYTKOWNIKA
    };
    int id;
    int idUzytkownika;
    int idPracownika;
    Kierunek kierunek;
    QString temat;
    QString tresc;

public:
    virtual ~Wiadomosc() noexcept = default;
    Wiadomosc() = default;
    Wiadomosc(const QString& dane);

    virtual QString Serializuj() const;
};

}}
