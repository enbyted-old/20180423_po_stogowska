#pragma once

#include <DA/Model/model.h>
#include <QString>

namespace DA { namespace Model {

class Historia : public Model
{
public:
    int id;
    int idFilmu;
    int idWypozyczajacego;
    QString dataWypozyczenia;
    QString dataZwrotu;

public:
    virtual ~Historia() = default;
    Historia() = default;
    Historia(const QString& dane);

    virtual QString Serializuj() const;
};

}}
