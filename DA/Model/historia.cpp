#include "historia.h"

#include <QStringList>
#include <QTextStream>
#include <stdexcept>

namespace DA { namespace Model {

Historia::Historia(const QString &dane)
{
    // Rozbija "dane" na tablicę kolejnych fragmentów między ';'
    QStringList czesci = dane.split(';');
    if (czesci.length() < 5)
        throw std::runtime_error("Nieprawidlowy rekord nośnika");

    id = czesci[0].toInt();
    idFilmu = czesci[1].toInt();
    idWypozyczajacego = czesci[2].toInt();
    dataWypozyczenia = czesci[3];
    dataZwrotu = czesci[4];
}

QString Historia::Serializuj() const
{
    QString dane;
    QTextStream str(&dane);

    str << id << ';';
    str << idFilmu << ';';
    str << idWypozyczajacego << ';';
    str << dataWypozyczenia << ';';
    str << dataZwrotu;

    return dane;
}

}}
