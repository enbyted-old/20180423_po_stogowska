#include "pracownik.h"

#include <QStringList>
#include <QTextStream>
#include <stdexcept>

namespace DA { namespace Model {

Pracownik::Pracownik(const QString &dane)
{
    // Rozbija "dane" na tablicę kolejnych fragmentów między ';'
    QStringList czesci = dane.split(';');
    if (czesci.length() < 5)
        throw std::runtime_error("Nieprawidlowy rekord pracownika");

    this->id = czesci[0].toInt();
    this->login = czesci[1];
    this->hashHasla = czesci[2];
    this->imie = czesci[3];
    this->nazwisko = czesci[4];
    this->uprawnienia = static_cast<PoziomUprawnien>(czesci[5].toInt());
}

QString Pracownik::Serializuj() const
{
    QString dane;
    QTextStream str(&dane);

    str << id << ';';
    str << login << ';';
    str << hashHasla << ';';
    str << imie << ';';
    str << nazwisko << ';';
    str << static_cast<int>(uprawnienia);

    return dane;
}

}}
