#pragma once

#include <QString>

namespace DA {
namespace Model {

class Logujacy
{
public:
    QString login;
    QString hashHasla;
public:
    virtual ~Logujacy() noexcept = default;
    bool SprawdzHaslo(const QString& haslo) const;
    void ZmienHaslo(const QString& noweHaslo);
protected:
    // Ta klasa nie może być użyta samodzielnie
    Logujacy();
private:
    QString Hash(const QString& tekst) const;
};

} // namespace Model
} // namespace DA
