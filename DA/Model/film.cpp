#include "film.h"

#include <QStringList>
#include <QTextStream>
#include <stdexcept>

namespace DA { namespace Model {

Film::Film(const QString &dane)
{
    // Rozbija "dane" na tablicę kolejnych fragmentów między ';'
    QStringList czesci = dane.split(';');
    if (czesci.length() < 11)
        throw std::runtime_error("Nieprawidlowy rekord filmu");

    id = czesci[0].toInt();
    kod = czesci[1];
    tytul = czesci[2];
    opis = czesci[3];
    rezyser = czesci[4];
    gatunek = czesci[5];
    dataPremiery = czesci[6];
    obsada = czesci[7];
    cena = czesci[8].toFloat();
    idWypozyczajacego = czesci[9].toInt();
    dataWypozyczenia = czesci[10];
}

QString Film::Serializuj() const
{
    QString dane;
    QTextStream str(&dane);

    str << id << ';';
    str << kod << ';';
    str << tytul << ';';
    str << opis << ';';
    str << rezyser << ';';
    str << gatunek << ';';
    str << dataPremiery << ';';
    str << obsada << ';';
    str << cena << ';';
    str << idWypozyczajacego << ';';
    str << dataWypozyczenia;

    return dane;
}

}}
