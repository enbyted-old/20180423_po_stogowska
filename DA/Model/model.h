#pragma once

#include <QString>

namespace DA {
namespace Model {

class Model
{
public:
    virtual ~Model() = default;
    virtual QString Serializuj() const = 0;
};

}}
