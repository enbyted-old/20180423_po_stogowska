#include "wiadomosc.h"

#include <QStringList>
#include <QTextStream>
#include <stdexcept>

namespace DA { namespace Model {

Wiadomosc::Wiadomosc(const QString &dane)
{
    // Rozbija "dane" na tablicę kolejnych fragmentów między ';'
    QStringList czesci = dane.split(';');
    if (czesci.length() < 6)
        throw std::runtime_error("Nieprawidlowy rekord wiadomości");

    this->id = czesci[0].toInt();
    this->idPracownika = czesci[1].toInt();
    this->idUzytkownika = czesci[2].toInt();
    this->kierunek = (Kierunek) czesci[3].toInt();
    // Zamiana bezpiecznego ciągu na oryginalny tekst
    this->temat = czesci[4].replace("\\:", ";").replace("\\\\", "\\");
    this->tresc = czesci[5].replace("\\n", "\n").replace("\\:", ";").replace("\\\\", "\\");
}

QString Wiadomosc::Serializuj() const
{
    QString dane;
    QTextStream str(&dane);

    str << id << ';';
    str << idPracownika << ';';
    str << idUzytkownika << ';';
    str << kierunek << ';';
    // Zamiana niebezpiecznych znaków na bezpieczny ciąg
    str << QString(temat).replace("\\", "\\").replace(";", "\\:") << ';';
    str << QString(tresc).replace("\r", "").replace("\\", "\\").replace("\n", "\\n").replace(";", "\\:");

    return dane;
}

}}
