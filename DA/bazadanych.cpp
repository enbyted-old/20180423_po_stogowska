#include "bazadanych.h"

#include <stdexcept>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <algorithm>

namespace DA {

void BazaDanych::CzytajCSV(const QString& nazwaPliku, std::function<void(const QString&)> funkcja)
{
    QFile plik(QDir::cleanPath(m_katalog + QDir::separator() + nazwaPliku));
    if (! plik.open(QFile::ReadWrite))
        throw std::runtime_error(qPrintable("Nie mozna wczytac pliku " + nazwaPliku));

    QTextStream str(&plik);
    while (! str.atEnd())
    {
        QString line = str.readLine().trimmed();
        if (line.length() > 0)
            funkcja(line);
    }

    plik.close();
}

void BazaDanych::ZapiszCSV(const QString& nazwaPliku, const std::vector<const DA::Model::Model*>& dane)
{
    QFile plik(QDir::cleanPath(m_katalog + QDir::separator() + nazwaPliku));
    if (! plik.open(QFile::WriteOnly | QFile::Truncate))
        throw std::runtime_error(qPrintable("Nie mozna zapisac pliku " + nazwaPliku));

    QTextStream str(&plik);
    for(const auto& rekord : dane)
    {
        str << rekord->Serializuj() << '\n';
    }

    plik.close();
}

void BazaDanych::Wczytaj()
{
    m_maxUzytkownikId = 0;
    m_uzytkownicy.clear();
    CzytajCSV("uzytkownicy.csv", [this](const QString& dane) {
        auto uzytkownik = Model::Uzytkownik(dane);

        if (uzytkownik.id > m_maxUzytkownikId)
            m_maxUzytkownikId = uzytkownik.id;

        m_uzytkownicy.push_back(uzytkownik);
    });

    m_maxPracownikId = 0;
    m_pracownicy.clear();
    CzytajCSV("pracownicy.csv", [this](const QString& dane) {
        auto pracownik = Model::Pracownik(dane);

        if (pracownik.id > m_maxPracownikId)
            m_maxPracownikId = pracownik.id;

        m_pracownicy.push_back(pracownik);
    });

    m_maxFilmId = 0;
    m_filmy.clear();
    CzytajCSV("filmy.csv", [this](const QString& dane) {
        auto film = Model::Film(dane);

        if (film.id > m_maxFilmId)
            m_maxFilmId = film.id;

        m_filmy.push_back(film);
    });

    m_maxHistoriaId = 0;
    m_historia.clear();
    CzytajCSV("historia.csv", [this](const QString& dane) {
        auto historia = Model::Historia(dane);

        if (historia.id > m_maxHistoriaId)
            m_maxHistoriaId = historia.id;

        m_historia.push_back(historia);
    });

    m_maxWiadomoscId = 0;
    m_wiadomosci.clear();
    CzytajCSV("wiadomosci.csv", [this](const QString& dane) {
        auto wiadomosc = Model::Wiadomosc(dane);

        if (wiadomosc.id > m_maxWiadomoscId)
            m_maxWiadomoscId = wiadomosc.id;

        m_wiadomosci.push_back(wiadomosc);
    });

    m_maxRecenzjaId = 0;
    m_recenzje.clear();
    CzytajCSV("recenzje.csv", [this](const QString& dane) {
        auto recenzja = Model::Recenzja(dane);

        if (recenzja.id > m_maxRecenzjaId)
            m_maxRecenzjaId = recenzja.id;

        m_recenzje.push_back(recenzja);
    });
}

void BazaDanych::Zapisz()
{
    std::vector<const DA::Model::Model*> temp;

    temp.clear();
    temp.resize(m_uzytkownicy.size());
    std::transform(m_uzytkownicy.begin(), m_uzytkownicy.end(), temp.begin(), [](const DA::Model::Uzytkownik& u) -> const DA::Model::Model* { return &u; });
    ZapiszCSV("uzytkownicy.csv", temp);

    temp.clear();
    temp.resize(m_pracownicy.size());
    std::transform(m_pracownicy.begin(), m_pracownicy.end(), temp.begin(), [](const DA::Model::Pracownik& p) -> const DA::Model::Model* { return &p; });
    ZapiszCSV("pracownicy.csv", temp);

    temp.clear();
    temp.resize(m_filmy.size());
    std::transform(m_filmy.begin(), m_filmy.end(), temp.begin(), [](const DA::Model::Film& f) -> const DA::Model::Model* { return &f; });
    ZapiszCSV("filmy.csv", temp);

    temp.clear();
    temp.resize(m_historia.size());
    std::transform(m_historia.begin(), m_historia.end(), temp.begin(), [](const DA::Model::Historia& f) -> const DA::Model::Model* { return &f; });
    ZapiszCSV("historia.csv", temp);

    temp.clear();
    temp.resize(m_wiadomosci.size());
    std::transform(m_wiadomosci.begin(), m_wiadomosci.end(), temp.begin(), [](const DA::Model::Wiadomosc& f) -> const DA::Model::Model* { return &f; });
    ZapiszCSV("wiadomosci.csv", temp);

    temp.clear();
    temp.resize(m_recenzje.size());
    std::transform(m_recenzje.begin(), m_recenzje.end(), temp.begin(), [](const DA::Model::Recenzja& f) -> const DA::Model::Model* { return &f; });
    ZapiszCSV("recenzje.csv", temp);
}

}
