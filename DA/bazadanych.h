#pragma once

#include <QString>
#include <DA/Model/pracownik.h>
#include <DA/Model/uzytkownik.h>
#include <DA/Model/film.h>
#include <DA/Model/historia.h>
#include <DA/Model/wiadomosc.h>
#include <DA/Model/recenzja.h>
#include <vector>
#include <functional>

#define __GETTER(TYPE, NAME, FIELD) \
    const std::vector<Model::TYPE>& NAME() const { return FIELD; } \
    std::vector<Model::TYPE>& NAME() { return FIELD; }

namespace DA {

    class BazaDanych
    {
        QString m_katalog;
        std::vector<Model::Uzytkownik> m_uzytkownicy;
        std::vector<Model::Pracownik>  m_pracownicy;
        std::vector<Model::Film>       m_filmy;
        std::vector<Model::Historia>   m_historia;
        std::vector<Model::Wiadomosc>  m_wiadomosci;
        std::vector<Model::Recenzja>   m_recenzje;
        int m_maxUzytkownikId;
        int m_maxPracownikId;
        int m_maxFilmId;
        int m_maxHistoriaId;
        int m_maxWiadomoscId;
        int m_maxRecenzjaId;
    public:
        BazaDanych(const QString& katalog)
            : m_katalog(katalog)
        {}

        void Wczytaj();
        void Zapisz();

        int NastepneIdUzytkownika() { return ++m_maxUzytkownikId; }
        int NastepneIdPracownika() { return ++m_maxPracownikId; }
        int NastepneIdFilmu() { return ++m_maxFilmId; }
        int NastepneIdHistorii() { return ++m_maxHistoriaId; }
        int NastepneIdHWiadomosci() { return ++m_maxWiadomoscId; }
        int NastepneIdRecenzji() { return ++m_maxRecenzjaId; }

        __GETTER(Uzytkownik, Uzytkownicy, m_uzytkownicy)
        __GETTER(Pracownik,  Pracownicy,  m_pracownicy)
        __GETTER(Film,       Filmy,       m_filmy)
        __GETTER(Historia,   Historia,    m_historia)
        __GETTER(Wiadomosc,  Wiadomosci,  m_wiadomosci)
        __GETTER(Recenzja,   Recenzje,    m_recenzje)

    private:
        // Czyta plik i dla każdej niepustej linii wywołuje "funkcja"
        void CzytajCSV(const QString& plik, std::function<void(const QString&)> funkcja);
        // Zapisuje wektor do pliku
        void ZapiszCSV(const QString& plik, const std::vector<const DA::Model::Model*>& dane);
    };

}

#undef __GETTER
