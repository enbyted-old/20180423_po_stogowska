#include "bazadanych.h"

#include <BL/Model/uzytkownik.h>
#include <algorithm>
#include <stdexcept>
#include <QDate>

namespace BL {

void BazaDanych::Wczytaj()
{
    m_BazaDanych.Wczytaj();

    // Opakowanie niskopoziomowych struktur danych logiką biznesową
    m_uzytkownicy.clear();
    for(auto& model : m_BazaDanych.Uzytkownicy())
        m_uzytkownicy.push_back(std::make_shared<Model::Uzytkownik>(model));

    m_filmy.clear();
    for(auto& model : m_BazaDanych.Filmy())
    {
        auto film = std::make_shared<Model::Film>(*this, model);
        film->m_self = film;
        m_filmy.push_back(film);

        if (model.idWypozyczajacego != -1)
        {
            auto uzytkownik_it = std::find_if(
                        m_uzytkownicy.begin(),
                        m_uzytkownicy.end(),
                        [model](std::shared_ptr<Model::Uzytkownik> u) { return u->Id() == model.idWypozyczajacego; }
            );

            // Nieprawidłowe wpisy są naprawiane
            if (uzytkownik_it == m_uzytkownicy.end())
            {
                model.idWypozyczajacego = -1;
                continue;
            }

            auto uzytkownik = *uzytkownik_it;
            uzytkownik->m_filmy.push_back(film);
            film->m_wypozyczajacy = uzytkownik;
        }

    }

    m_pracownicy.clear();
    for(auto& model : m_BazaDanych.Pracownicy())
        m_pracownicy.push_back(std::make_shared<Model::Pracownik>(model));

    m_historia.clear();
    for(auto& model : m_BazaDanych.Historia())
    {
        // Wyszukanie filmu powiązanego z wpisem
        auto film_it = std::find_if(
                    m_filmy.begin(),
                    m_filmy.end(),
                    [model](std::shared_ptr<Model::Film> f) { return f->Id() == model.idFilmu; }
        );

        // Nieprawidłowe wpisy sa pomijane
        if (film_it == m_filmy.end())
            continue;

        // Wyszukanie użytkownika powiązanego z wpisem
        auto uzytkownik_it = std::find_if(
                    m_uzytkownicy.begin(),
                    m_uzytkownicy.end(),
                    [model](std::shared_ptr<Model::Uzytkownik> u) { return u->Id() == model.idWypozyczajacego; }
        );

        // Nieprawidłowe wpisy sa pomijane
        if (uzytkownik_it == m_uzytkownicy.end())
            continue;

        auto historia = std::make_shared<Model::Historia>(model);
        auto film = *film_it;
        auto uzytkownik = *uzytkownik_it;

        historia->m_film = film;
        historia->m_wypozyczajacy = uzytkownik;

        m_historia.push_back(historia);
        film->m_historia.push_back(historia);
        uzytkownik->m_historia.push_back(historia);
    }

    m_wiadomosci.clear();
    for (auto& model : m_BazaDanych.Wiadomosci())
    {
        // Wyszukanie użytkownika powiązanego z wiadomością
        auto pracownik_it = std::find_if(
                    m_pracownicy.begin(),
                    m_pracownicy.end(),
                    [model](std::shared_ptr<Model::Pracownik> p) { return p->Id() == model.idPracownika; }
        );

        // Nieprawidłowe wpisy sa pomijane
        if (pracownik_it == m_pracownicy.end())
            continue;

        // Wyszukanie użytkownika powiązanego z wiadomością
        auto uzytkownik_it = std::find_if(
                    m_uzytkownicy.begin(),
                    m_uzytkownicy.end(),
                    [model](std::shared_ptr<Model::Uzytkownik> u) { return u->Id() == model.idUzytkownika; }
        );

        // Nieprawidłowe wpisy sa pomijane
        if (uzytkownik_it == m_uzytkownicy.end())
            continue;

        auto wiadomosc = std::make_shared<Model::Wiadomosc>(model);
        auto pracownik = *pracownik_it;
        auto uzytkownik = *uzytkownik_it;

        wiadomosc->m_pracownik = pracownik;
        wiadomosc->m_uzytkownik = uzytkownik;

        m_wiadomosci.push_back(wiadomosc);
        pracownik->m_wiadomosci.push_back(wiadomosc);
        uzytkownik->m_wiadomosci.push_back(wiadomosc);
    }

    m_recenzje.clear();
    for (auto& model : m_BazaDanych.Recenzje())
    {
        // Wyszukanie filmu powiązanego z wpisem
        auto film_it = std::find_if(
                    m_filmy.begin(),
                    m_filmy.end(),
                    [model](std::shared_ptr<Model::Film> f) { return f->Id() == model.idFilmu; }
        );

        // Nieprawidłowe wpisy sa pomijane
        if (film_it == m_filmy.end())
            continue;

        // Wyszukanie użytkownika powiązanego z wpisem
        auto uzytkownik_it = std::find_if(
                    m_uzytkownicy.begin(),
                    m_uzytkownicy.end(),
                    [model](std::shared_ptr<Model::Uzytkownik> u) { return u->Id() == model.idUzytkownika; }
        );

        // Nieprawidłowe wpisy sa pomijane
        if (uzytkownik_it == m_uzytkownicy.end())
            continue;

        auto recenzja = std::make_shared<Model::Recenzja>(model);
        auto uzytkownik = *uzytkownik_it;
        auto film = *film_it;

        recenzja->m_uzytkownik = uzytkownik;
        recenzja->m_film = film;
        m_recenzje.push_back(recenzja);
        uzytkownik->m_recenzje.push_back(recenzja);
        film->m_recenzje.push_back(recenzja);
    }
}

void BazaDanych::Zapisz()
{
    m_BazaDanych.Uzytkownicy().clear();
    m_BazaDanych.Uzytkownicy().resize(m_uzytkownicy.size());
    std::transform(
                m_uzytkownicy.begin(),
                m_uzytkownicy.end(),
                m_BazaDanych.Uzytkownicy().begin(),
                [](std::shared_ptr<Model::Uzytkownik> u) { return u->m_model; }
                );

    m_BazaDanych.Pracownicy().clear();
    m_BazaDanych.Pracownicy().resize(m_pracownicy.size());
    std::transform(
                m_pracownicy.begin(),
                m_pracownicy.end(),
                m_BazaDanych.Pracownicy().begin(),
                [](std::shared_ptr<Model::Pracownik> u) { return u->m_model; }
                );

    m_BazaDanych.Filmy().clear();
    m_BazaDanych.Filmy().resize(m_filmy.size());
    std::transform(
                m_filmy.begin(),
                m_filmy.end(),
                m_BazaDanych.Filmy().begin(),
                [](std::shared_ptr<Model::Film> u) { return u->m_model; }
                );

    m_BazaDanych.Historia().clear();
    m_BazaDanych.Historia().resize(m_historia.size());
    std::transform(
                m_historia.begin(),
                m_historia.end(),
                m_BazaDanych.Historia().begin(),
                [](std::shared_ptr<Model::Historia> u) { return u->m_model; }
                );

    m_BazaDanych.Wiadomosci().clear();
    m_BazaDanych.Wiadomosci().resize(m_wiadomosci.size());
    std::transform(
                m_wiadomosci.begin(),
                m_wiadomosci.end(),
                m_BazaDanych.Wiadomosci().begin(),
                [](std::shared_ptr<Model::Wiadomosc> w) { return w->m_model; }
                );

    m_BazaDanych.Recenzje().clear();
    m_BazaDanych.Recenzje().resize(m_recenzje.size());
    std::transform(
                m_recenzje.begin(),
                m_recenzje.end(),
                m_BazaDanych.Recenzje().begin(),
                [](std::shared_ptr<Model::Recenzja> r) { return r->m_model; }
                );

    m_BazaDanych.Zapisz();
}

void BazaDanych::RejestrujUzytkownika(const QString &login, const QString &haslo, const QString &imie, const QString &nazwisko)
{
    auto it = std::find_if(
                m_BazaDanych.Uzytkownicy().begin(),
                m_BazaDanych.Uzytkownicy().end(),
                [login](DA::Model::Uzytkownik& u) { return u.login == login; }
    );
    if (it != m_BazaDanych.Uzytkownicy().end()) {
        throw std::runtime_error("Użytkownik o takim loginie już istnieje!");
    }

    DA::Model::Uzytkownik uzytkownik;
    uzytkownik.login = login;
    uzytkownik.imie = imie;
    uzytkownik.nazwisko = nazwisko;
    uzytkownik.id = m_BazaDanych.NastepneIdUzytkownika();
    uzytkownik.ZmienHaslo(haslo);

    m_uzytkownicy.push_back(std::make_shared<Model::Uzytkownik>(uzytkownik));
}

std::shared_ptr<Model::Uzytkownik> BazaDanych::ZalogujUzytkownika(const QString &login, const QString &haslo)
{
    for(auto uzytkownik : m_uzytkownicy)
    {
        if (uzytkownik->Login() != login)
            continue;

        if (! uzytkownik->SprawdzHaslo(haslo))
            return std::shared_ptr<Model::Uzytkownik>();

        return uzytkownik;
    }

    return std::shared_ptr<Model::Uzytkownik>();
}

void BazaDanych::UsunUzytkownika(std::shared_ptr<Model::Uzytkownik> uzytkownik)
{
    // Usuń użytkownika
    m_uzytkownicy.erase(
        std::remove_if(
            m_uzytkownicy.begin(),
            m_uzytkownicy.end(),
            [uzytkownik] (std::shared_ptr<Model::Uzytkownik> u) { return u->Id() == uzytkownik->Id(); }
        ),
        m_uzytkownicy.end()
    );

    // Usuń skojarzone z nim wpisy historii
    m_historia.erase(
        std::remove_if(
            m_historia.begin(),
            m_historia.end(),
            [uzytkownik] (std::shared_ptr<Model::Historia> h) { return h->Uzytkownik()->Id() == uzytkownik->Id(); }
        ),
        m_historia.end()
    );

    // Usuń skojarzone z nim wiadomości
    m_wiadomosci.erase(
        std::remove_if(
            m_wiadomosci.begin(),
            m_wiadomosci.end(),
            [uzytkownik] (std::shared_ptr<Model::Wiadomosc> w) { return w->Uzytkownik()->Id() == uzytkownik->Id(); }
        ),
        m_wiadomosci.end()
            );
}

void BazaDanych::DodajPracownika(const QString &login, const QString &haslo, const QString &imie, const QString &nazwisko, DA::Model::Pracownik::PoziomUprawnien uprawnienia)
{
    auto it = std::find_if(
                m_BazaDanych.Pracownicy().begin(),
                m_BazaDanych.Pracownicy().end(),
                [login](DA::Model::Pracownik& p) { return p.login == login; }
    );

    if (it != m_BazaDanych.Pracownicy().end()) {
        throw std::runtime_error("Pracownik o takim loginie już istnieje!");
    }

    DA::Model::Pracownik pracownik;
    pracownik.login = login;
    pracownik.imie = imie;
    pracownik.nazwisko = nazwisko;
    pracownik.id = m_BazaDanych.NastepneIdPracownika();
    pracownik.uprawnienia = uprawnienia;
    pracownik.ZmienHaslo(haslo);

    m_pracownicy.push_back(std::make_shared<Model::Pracownik>(pracownik));
}

std::shared_ptr<Model::Pracownik> BazaDanych::ZalogujPracownika(const QString &login, const QString &haslo)
{
    for(auto& pracownik : m_pracownicy)
    {
        if (pracownik->Login() != login)
            continue;

        if (! pracownik->SprawdzHaslo(haslo))
            return std::shared_ptr<Model::Pracownik>();

        return pracownik;
    }

    return std::shared_ptr<Model::Pracownik>();
}

void BazaDanych::UsunPracownika(std::shared_ptr<Model::Pracownik> pracownik)
{
    // Usuń pracownika
    m_pracownicy.erase(
        std::remove_if(
            m_pracownicy.begin(),
            m_pracownicy.end(),
            [pracownik] (std::shared_ptr<Model::Pracownik> p) { return p->Id() == pracownik->Id(); }
        ),
        m_pracownicy.end()
    );

    // Usuń skojarzone z nim wiadomości
    m_wiadomosci.erase(
        std::remove_if(
            m_wiadomosci.begin(),
            m_wiadomosci.end(),
            [pracownik] (std::shared_ptr<Model::Wiadomosc> w) { return w->Pracownik()->Id() == pracownik->Id(); }
        ),
        m_wiadomosci.end()
    );
}

std::shared_ptr<Model::Film> BazaDanych::FilmPoNumerzeWiersza(std::size_t id)
{
    if (id >= m_filmy.size())
        return std::shared_ptr<Model::Film>();

    return m_filmy.at(id);
}

void BazaDanych::DodajFilm(
        const QString &tytul,
        const QString &kod,
        const QString &gatunek,
        const QString &dataPremiery,
        const QString &rezyser,
        const QString &obsada,
        const QString &opis,
        float cena)
{
    DA::Model::Film model;
    model.id = m_BazaDanych.NastepneIdFilmu();
    model.tytul = tytul;
    model.kod = kod;
    model.gatunek = gatunek;
    model.dataPremiery = dataPremiery;
    model.rezyser = rezyser;
    model.obsada = obsada;
    model.opis = opis;
    model.cena = cena;
    model.idWypozyczajacego = -1;

    auto film = std::make_shared<Model::Film>(*this, model);
    film->m_self = film;
    m_filmy.push_back(film);
}

std::shared_ptr<Model::Recenzja> BazaDanych::RecenzjaPoNumerzeWiersza(std::size_t id)
{
    if (id >= m_recenzje.size())
        return std::shared_ptr<Model::Recenzja>();

    return m_recenzje.at(id);
}

void BazaDanych::DodajRecenzje(std::shared_ptr<Model::Uzytkownik> uzytkownik, std::shared_ptr<Model::Film> film, int ocena, const QString &tresc)
{
    DA::Model::Recenzja model;
    model.id = m_BazaDanych.NastepneIdRecenzji();
    model.idUzytkownika = uzytkownik->Id();
    model.idFilmu = film->Id();
    model.ocena = ocena;
    model.tresc = tresc;

    auto recenzja = std::make_shared<Model::Recenzja>(model);
    m_recenzje.push_back(recenzja);
    uzytkownik->m_recenzje.push_back(recenzja);
    film->m_recenzje.push_back(recenzja);
}

std::shared_ptr<Model::Uzytkownik> BazaDanych::UzytkownikPoNumerzeWiersza(std::size_t id)
{
    if (id >= m_uzytkownicy.size())
        return std::shared_ptr<Model::Uzytkownik>();

    return m_uzytkownicy.at(id);
}

int BazaDanych::IndeksUzytkownika(std::shared_ptr<Model::Uzytkownik> uzytkownik)
{
    auto it = std::find(m_uzytkownicy.begin(), m_uzytkownicy.end(), uzytkownik);
    if (it == m_uzytkownicy.end())
        return -1;

    return std::distance(m_uzytkownicy.begin(), it);
}

std::shared_ptr<Model::Pracownik> BazaDanych::PracownikPoNumerzeWiersza(std::size_t id)
{
    if (id >= m_pracownicy.size())
        return std::shared_ptr<Model::Pracownik>();

    return m_pracownicy.at(id);
}

std::shared_ptr<Model::Pracownik> BazaDanych::PracownikPoLoginie(const QString &login)
{
    auto it = std::find_if(
        m_pracownicy.begin(),
        m_pracownicy.end(),
        [login] (std::shared_ptr<Model::Pracownik> p) { return p->Login() == login; }
    );

    if (it == m_pracownicy.end())
        return std::shared_ptr<Model::Pracownik>();

    return *it;
}

int BazaDanych::IndeksPracownika(std::shared_ptr<Model::Pracownik> pracownik)
{
    auto it = std::find(m_pracownicy.begin(), m_pracownicy.end(), pracownik);
    if (it == m_pracownicy.end())
        return -1;

    return std::distance(m_pracownicy.begin(), it);
}

void BazaDanych::DodajWpisHistorii(
        std::shared_ptr<Model::Uzytkownik> wypozyczajacy,
        std::shared_ptr<Model::Film> film,
        const QString &dataWypozyczenia,
        const QString &dataZwrotu)
{
    auto wpis = std::make_shared<Model::Historia>(m_BazaDanych.NastepneIdHistorii(), film, wypozyczajacy, dataWypozyczenia, dataZwrotu);
    m_historia.push_back(wpis);
    wypozyczajacy->m_historia.push_back(wpis);
    film->m_historia.push_back(wpis);
}

void BazaDanych::WyslijWiadomosc(
        std::shared_ptr<Model::Uzytkownik> uzytkownik,
        std::shared_ptr<Model::Pracownik> pracownik,
        DA::Model::Wiadomosc::Kierunek kierunek,
        const QString &temat,
        const QString &tresc)
{
    DA::Model::Wiadomosc model;
    model.id = m_BazaDanych.NastepneIdHWiadomosci();
    model.idPracownika = pracownik->Id();
    model.idUzytkownika = uzytkownik->Id();
    model.kierunek = kierunek;
    model.temat = temat;
    model.tresc = tresc;

    auto wiadomosc = std::make_shared<Model::Wiadomosc>(model);
    wiadomosc->m_uzytkownik = uzytkownik;
    wiadomosc->m_pracownik = pracownik;
    m_wiadomosci.push_back(wiadomosc);
    uzytkownik->m_wiadomosci.push_back(wiadomosc);
    pracownik->m_wiadomosci.push_back(wiadomosc);
}

}
