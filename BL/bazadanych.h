#pragma once

#include <QString>
#include <vector>
#include <memory>
#include <DA/bazadanych.h>
#include <BL/Model/uzytkownik.h>
#include <BL/Model/pracownik.h>
#include <BL/Model/film.h>
#include <BL/Model/historia.h>
#include <BL/Model/wiadomosc.h>
#include <BL/Model/recenzja.h>

namespace BL {

class BazaDanych
{
    DA::BazaDanych m_BazaDanych;
    std::vector<std::shared_ptr<Model::Uzytkownik>> m_uzytkownicy;
    std::vector<std::shared_ptr<Model::Pracownik>>  m_pracownicy;
    std::vector<std::shared_ptr<Model::Film>>       m_filmy;
    std::vector<std::shared_ptr<Model::Historia>>   m_historia;
    std::vector<std::shared_ptr<Model::Wiadomosc>>  m_wiadomosci;
    std::vector<std::shared_ptr<Model::Recenzja>>   m_recenzje;
public:
    BazaDanych(const QString& katalog)
        : m_BazaDanych(katalog)
    {}

    void Wczytaj();
    void Zapisz();

    void RejestrujUzytkownika(const QString& login, const QString& haslo, const QString& imie, const QString& nazwisko);
    std::shared_ptr<Model::Uzytkownik> ZalogujUzytkownika(const QString& login, const QString& haslo);
    void UsunUzytkownika(std::shared_ptr<Model::Uzytkownik> uzytkownik);
    void DodajPracownika(const QString& login, const QString& haslo, const QString& imie, const QString& nazwisko, DA::Model::Pracownik::PoziomUprawnien uprawnienia);
    std::shared_ptr<Model::Pracownik> ZalogujPracownika(const QString& login, const QString& haslo);
    void UsunPracownika(std::shared_ptr<Model::Pracownik> pracownik);

    std::shared_ptr<Model::Film> FilmPoNumerzeWiersza(std::size_t id);
    int IloscFilmow() const { return m_filmy.size(); }
    void DodajFilm(
            const QString& tytul,
            const QString& kod,
            const QString& gatunek,
            const QString& dataPremiery,
            const QString& rezyser,
            const QString& obsada,
            const QString& opis,
            float cena);

    std::shared_ptr<Model::Recenzja> RecenzjaPoNumerzeWiersza(std::size_t id);
    int IloscRecenzji() const { return m_recenzje.size(); }
    void DodajRecenzje(
            std::shared_ptr<Model::Uzytkownik> uzytkownik,
            std::shared_ptr<Model::Film> film,
            int ocena,
            const QString& tresc
            );

    std::shared_ptr<Model::Uzytkownik> UzytkownikPoNumerzeWiersza(std::size_t id);
    int IndeksUzytkownika(std::shared_ptr<Model::Uzytkownik> uzytkownik);
    int IloscUzytkownikow() const { return m_uzytkownicy.size(); }

    std::shared_ptr<Model::Pracownik> PracownikPoNumerzeWiersza(std::size_t id);
    std::shared_ptr<Model::Pracownik> PracownikPoLoginie(const QString& login);
    int IndeksPracownika(std::shared_ptr<Model::Pracownik> pracownik);
    int IloscPracownikow() const { return m_pracownicy.size(); }

    void DodajWpisHistorii(
            std::shared_ptr<Model::Uzytkownik> wypozyczajacy,
            std::shared_ptr<Model::Film> film,
            const QString& dataWypozyczenia,
            const QString& dataZwrotu);

    void WyslijWiadomosc(
            std::shared_ptr<Model::Uzytkownik> uzytkownik,
            std::shared_ptr<Model::Pracownik> pracownik,
            DA::Model::Wiadomosc::Kierunek kierunek,
            const QString& temat,
            const QString& tresc
            );
};

}
