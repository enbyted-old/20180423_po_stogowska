#pragma once

#include <DA/Model/wiadomosc.h>
#include <memory>

#define __GET_SET(TYPE, NAME, FIELD) \
    const TYPE& NAME() const { return FIELD; } \
    void NAME(const TYPE& wartosc) { FIELD = wartosc; }

#define __GET(TYPE, NAME, FIELD) \
    const TYPE& NAME() const { return FIELD; }

namespace BL
{
    class BazaDanych;
}

namespace BL { namespace Model {

class Uzytkownik;
class Pracownik;

class Wiadomosc
{
    friend class BL::BazaDanych;
    DA::Model::Wiadomosc m_model;
    std::weak_ptr<Uzytkownik> m_uzytkownik;
    std::weak_ptr<Pracownik> m_pracownik;
public:
    Wiadomosc() = default;
    Wiadomosc(const DA::Model::Wiadomosc& model)
        : m_model(model)
    {}

    __GET(int, Id, m_model.id)
    __GET(DA::Model::Wiadomosc::Kierunek, Kierunek, m_model.kierunek)
    std::shared_ptr<Uzytkownik> Uzytkownik() { return m_uzytkownik.lock(); }
    std::shared_ptr<Pracownik> Pracownik() { return m_pracownik.lock(); }
    __GET(QString, Temat, m_model.temat)
    __GET(QString, Tresc, m_model.tresc)
};

}}

#undef __GET_SET
#undef __GET
