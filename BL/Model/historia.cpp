#include "historia.h"
#include "film.h"
#include "uzytkownik.h"

namespace BL { namespace Model {

Historia::Historia(
        int id,
        std::shared_ptr<Model::Film> film,
        std::shared_ptr<Model::Uzytkownik> uzytkownik,
        const QString &dataWypozyczenia,
        const QString &dataZwrotu)
{
    m_film = film;
    m_wypozyczajacy = uzytkownik;

    DA::Model::Historia model;
    model.id = id;
    model.idFilmu = film->Id();
    model.idWypozyczajacego = uzytkownik->Id();
    model.dataWypozyczenia = dataWypozyczenia;
    model.dataZwrotu = dataZwrotu;

    m_model = model;
}

}}
