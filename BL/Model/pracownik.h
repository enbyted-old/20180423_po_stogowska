#pragma once

#include <DA/Model/pracownik.h>
#include <vector>
#include <memory>

#define __GET_SET(TYPE, NAME, FIELD) \
    const TYPE& NAME() const { return FIELD; } \
    void NAME(const TYPE& wartosc) { FIELD = wartosc; }

#define __GET(TYPE, NAME, FIELD) \
    const TYPE& NAME() const { return FIELD; }

namespace BL
{
    class BazaDanych;
}

namespace BL { namespace Model {

class Wiadomosc;

class Pracownik
{
    friend class BL::BazaDanych;
    DA::Model::Pracownik m_model;
    std::vector<std::shared_ptr<Wiadomosc>> m_wiadomosci;
public:
    Pracownik() = default;

    Pracownik(const DA::Model::Pracownik& model)
        : m_model(model)
    {}

    __GET(int, Id, m_model.id)
    __GET_SET(QString, Login, m_model.login)
    __GET_SET(QString, Imie, m_model.imie)
    __GET_SET(QString, Nazwisko, m_model.nazwisko)
    __GET_SET(DA::Model::Pracownik::PoziomUprawnien, Uprawnienia, m_model.uprawnienia)
    __GET(std::vector<std::shared_ptr<Wiadomosc>>, Wiadomosci, m_wiadomosci)
    QString ImieNazwisko() const { return m_model.imie + " " + m_model.nazwisko; }
    bool SprawdzHaslo(const QString& haslo) { return m_model.SprawdzHaslo(haslo); }
    void ZmienHaslo(const QString& noweHaslo) { return m_model.ZmienHaslo(noweHaslo); }
};

}}

#undef __GET_SET
#undef __GET
