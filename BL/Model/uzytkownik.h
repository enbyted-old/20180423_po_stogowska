#pragma once

#include <DA/Model/uzytkownik.h>
#include <vector>
#include <memory>

#define __GET_SET(TYPE, NAME, FIELD) \
    const TYPE& NAME() const { return FIELD; } \
    void NAME(const TYPE& wartosc) { FIELD = wartosc; }

#define __GET(TYPE, NAME, FIELD) \
    const TYPE& NAME() const { return FIELD; }

namespace BL
{
    class BazaDanych;
}

namespace BL { namespace Model {

class Film;
class Recenzja;
class Historia;
class Wiadomosc;

class Uzytkownik
{
    friend class BL::BazaDanych;
    friend class Film;
    DA::Model::Uzytkownik m_model;
    std::vector<std::shared_ptr<Historia>>  m_historia;
    std::vector<std::shared_ptr<Film>>      m_filmy;
    std::vector<std::shared_ptr<Recenzja>>  m_recenzje;
    std::vector<std::shared_ptr<Wiadomosc>> m_wiadomosci;
public:
    Uzytkownik(const DA::Model::Uzytkownik& model)
        : m_model(model)
    {}

    __GET(int, Id, m_model.id)
    __GET(QString, Login, m_model.login)
    __GET_SET(QString, Imie, m_model.imie)
    __GET_SET(QString, Nazwisko, m_model.nazwisko)
    __GET(std::vector<std::shared_ptr<Historia>>,  Historia,   m_historia)
    __GET(std::vector<std::shared_ptr<Film>>,      Filmy,      m_filmy)
    __GET(std::vector<std::shared_ptr<Recenzja>>,  Recenzje,   m_recenzje)
    __GET(std::vector<std::shared_ptr<Wiadomosc>>, Wiadomosci, m_wiadomosci)
    QString ImieNazwisko() const { return m_model.imie + " " + m_model.nazwisko; }
    bool SprawdzHaslo(const QString& haslo) { return m_model.SprawdzHaslo(haslo); }
    void ZmienHaslo(const QString& noweHaslo) { return m_model.ZmienHaslo(noweHaslo); }
};

}}

#undef __GET_SET
#undef __GET
