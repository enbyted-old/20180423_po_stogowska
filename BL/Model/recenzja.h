#pragma once

#include <DA/Model/recenzja.h>
#include <memory>

#define __GET_SET(TYPE, NAME, FIELD) \
    const TYPE& NAME() const { return FIELD; } \
    void NAME(const TYPE& wartosc) { FIELD = wartosc; }

#define __GET(TYPE, NAME, FIELD) \
    const TYPE& NAME() const { return FIELD; }

namespace BL
{
    class BazaDanych;
}

namespace BL { namespace Model {

class Uzytkownik;
class Film;

class Recenzja
{
    friend class BL::BazaDanych;
    DA::Model::Recenzja m_model;
    std::weak_ptr<Uzytkownik> m_uzytkownik;
    std::weak_ptr<Film> m_film;
public:
    Recenzja() = default;
    Recenzja(const DA::Model::Recenzja& model)
        : m_model(model)
    {}

    __GET(int, Id, m_model.id)
    std::shared_ptr<Uzytkownik> Uzytkownik() { return m_uzytkownik.lock(); }
    std::shared_ptr<Film> Film() { return m_film.lock(); }
    __GET_SET(int, Ocena, m_model.ocena)
    __GET_SET(QString, Tresc, m_model.tresc)
};

}}

#undef __GET_SET
#undef __GET
