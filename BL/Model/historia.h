#pragma once

#include <DA/Model/historia.h>
#include <memory>

#define __GET_SET(TYPE, NAME, FIELD) \
    const TYPE& NAME() const { return FIELD; } \
    void NAME(const TYPE& wartosc) { FIELD = wartosc; }

#define __GET(TYPE, NAME, FIELD) \
    const TYPE& NAME() const { return FIELD; }

namespace BL
{
    class BazaDanych;
}

namespace BL { namespace Model {

class Film;
class Uzytkownik;

class Historia
{
    friend class BL::BazaDanych;
    DA::Model::Historia m_model;
    std::weak_ptr<Model::Film> m_film;
    std::weak_ptr<Model::Uzytkownik> m_wypozyczajacy;
public:
    Historia() = default;

    Historia(const DA::Model::Historia& model)
        : m_model(model)
    {}

    Historia(
            int id,
            std::shared_ptr<Model::Film> film,
            std::shared_ptr<Model::Uzytkownik> uzytkownik,
            const QString& dataWypozyczenia,
            const QString& dataZwrotu
    );

    __GET(int, Id, m_model.id)
    std::shared_ptr<Film> Film() const { return m_film.lock(); }
    std::shared_ptr<Uzytkownik> Uzytkownik() const { return m_wypozyczajacy.lock(); }
    __GET_SET(QString, DataWypozyczenia, m_model.dataWypozyczenia)
    __GET_SET(QString, DataZwrotu, m_model.dataZwrotu)
};

}}

#undef __GET_SET
#undef __GET
