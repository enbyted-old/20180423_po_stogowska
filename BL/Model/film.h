#pragma once

#include <DA/Model/film.h>
#include <vector>
#include <memory>

namespace BL
{
    class BazaDanych;
}

namespace BL { namespace Model {

#define __GET_SET(TYPE, NAME, FIELD) \
    const TYPE& NAME() const { return FIELD; } \
    void NAME(const TYPE& wartosc) { FIELD = wartosc; }

#define __GET(TYPE, NAME, FIELD) \
    const TYPE& NAME() const { return FIELD; }

class Historia;
class Uzytkownik;
class Recenzja;

class Film
{
    friend class BL::BazaDanych;
    DA::Model::Film m_model;
    std::weak_ptr<Uzytkownik> m_wypozyczajacy;
    std::vector<std::shared_ptr<Historia>> m_historia;
    std::vector<std::shared_ptr<Recenzja>> m_recenzje;
    std::weak_ptr<Film> m_self;
    BL::BazaDanych& m_bazaDanych;
public:
    Film(BL::BazaDanych& bazaDanych, const DA::Model::Film& model)
        : m_model(model)
        , m_bazaDanych(bazaDanych)
    {}

    __GET(int, Id, m_model.id)
    __GET_SET(QString, Kod, m_model.kod)
    __GET_SET(QString, Tytul, m_model.tytul)
    __GET_SET(QString, Opis, m_model.opis)
    __GET_SET(QString, Rezyser, m_model.rezyser)
    __GET_SET(QString, Gatunek, m_model.gatunek)
    __GET_SET(QString, DataPremiery, m_model.dataPremiery)
    __GET_SET(QString, Obsada, m_model.obsada)
    __GET_SET(float, Cena, m_model.cena)
    __GET(QString, DataWypozyczenia, m_model.dataWypozyczenia)
    std::shared_ptr<Uzytkownik> Wypozyczajacy() { return m_wypozyczajacy.lock(); }
    __GET(std::vector<std::shared_ptr<Historia>>, Historia, m_historia)
    __GET(std::vector<std::shared_ptr<Recenzja>>,  Recenzje,   m_recenzje)

    void Wypozycz(std::shared_ptr<Uzytkownik> wypozyczajacy);
    void Zwroc(int ocena, const QString& recenzja);
    void Zwroc();
    float Ocena();
};

}}

#undef __GET
#undef __GET_SET
