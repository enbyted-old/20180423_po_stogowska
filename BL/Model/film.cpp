#include "film.h"
#include "uzytkownik.h"
#include <BL/bazadanych.h>
#include <QDate>
#include <numeric>

namespace BL { namespace Model {

void Film::Wypozycz(std::shared_ptr<Uzytkownik> uzytkownik)
{
    if (! m_wypozyczajacy.expired())
        throw std::runtime_error("Ten film już jest wypożyczony!");

    m_wypozyczajacy = uzytkownik;
    uzytkownik->m_filmy.push_back(m_self.lock());
    m_model.idWypozyczajacego = uzytkownik->Id();
    m_model.dataWypozyczenia = QDate::currentDate().toString();
}

void Film::Zwroc(int ocena, const QString &recenzja)
{
    auto wypozyczajacy = m_wypozyczajacy.lock();
    if (! wypozyczajacy)
        throw std::runtime_error("Ten film nie jest wypożyczony!");

    m_bazaDanych.DodajWpisHistorii(wypozyczajacy, m_self.lock(), DataWypozyczenia(), QDate::currentDate().toString());
    m_bazaDanych.DodajRecenzje(wypozyczajacy, m_self.lock(), ocena, recenzja);
    auto it = std::remove_if(
                wypozyczajacy->m_filmy.begin(),
                wypozyczajacy->m_filmy.end(),
                [this](std::shared_ptr<Model::Film> f) { return f->Id() == this->Id(); }
    );

    wypozyczajacy->m_filmy.erase(it,  wypozyczajacy->m_filmy.end());
    m_wypozyczajacy.reset();
    m_model.idWypozyczajacego = -1;
}

void Film::Zwroc()
{
    auto wypozyczajacy = m_wypozyczajacy.lock();
    if (! wypozyczajacy)
        throw std::runtime_error("Ten film nie jest wypożyczony!");

    m_bazaDanych.DodajWpisHistorii(wypozyczajacy, m_self.lock(), DataWypozyczenia(), QDate::currentDate().toString());
    auto it = std::remove_if(
                wypozyczajacy->m_filmy.begin(),
                wypozyczajacy->m_filmy.end(),
                [this](std::shared_ptr<Model::Film> f) { return f->Id() == this->Id(); }
    );

    wypozyczajacy->m_filmy.erase(it,  wypozyczajacy->m_filmy.end());
    m_wypozyczajacy.reset();
    m_model.idWypozyczajacego = -1;
}

float Film::Ocena()
{
    if (m_recenzje.size() == 0)
        return 0;

    std::vector<int> oceny;
    oceny.resize(m_recenzje.size());
    // Wyciągnij oceny z recenzji
    std::transform(m_recenzje.begin(), m_recenzje.end(), oceny.begin(), [](std::shared_ptr<Recenzja> r) { return r->Ocena(); });
    int sumaOcen = std::accumulate(oceny.begin(), oceny.end(), 0);

    return static_cast<float>(sumaOcen) / static_cast<float>(oceny.size());
}

}}
